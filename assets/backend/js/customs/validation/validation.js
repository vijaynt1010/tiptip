$(document).ready(function() {
	$('#loginForm').bootstrapValidator({
		/*feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},*/
		fields: {
			user_name: {
				validators: {
					notEmpty: {
						message: 'Please enter the user name'
					}
				}
			},
			user_password: {
				validators: {
					notEmpty: {
						message: 'Please enter the password'
					}
				}
			}
		}
	});	
          
    $('#changePasswordForm').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			current_password: {
				validators: {
					notEmpty: {
						message: 'Please enter the current password'
					}
				}
			},
			new_password: {
				validators: {
					notEmpty: {
						message: 'Please enter the current password'
					},securePassword :{
						message: 'The new password is not valid'
					}
				}
			},
			confirm_newpassword: {
                message: 'The confirm password is not valid',
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required'
                    },
                    identical : {
                        field : 'new_password',
                        message: 'The passwords is do not match'
                    }
                }
            }			
			
		}		
	});      
    
    $('#profileUpdateForm').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			first_name: {
				validators: {
					notEmpty: {
						message: 'Please enter the first name'
					}
				}
			},
			last_name: {
				validators: {
					notEmpty: {
						message: 'Please enter the last name'
					}
				}
			},
			user_name: {               
                validators: {
                    notEmpty: {
                        message: 'Please enter the user name'
                    },remote: {
                        url: base_url+'myaccount/is_username_available',                        
                        message: 'The user name is not available'
                    }
                }
            },
			email_id: {               
                validators: {
                    notEmpty: {
                        message: 'Please enter the email'
                    },emailAddress: {
                        message: 'Please enter a valid email'
                    },remote: {
                        url: base_url+'myaccount/is_email_available',                        
                        message: 'The user name is not available'
                    }
                }
            }					
		}		
	});
    
   $('#tipsterAddForm').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			tipster_name: {
				validators: {
					notEmpty: {
						message: 'Please enter the tipster name'
					}
				}
			},
			tipster_status: {
				validators: {
					notEmpty: {
						message: 'Please select the tipster status'
					}
				}
			}				
		}		
	});
   
   $('#tipsterEditForm').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			tipster_name: {
				validators: {
					notEmpty: {
						message: 'Please enter the tipster name'
					}
				}
			},
			tipster_status: {
				validators: {
					notEmpty: {
						message: 'Please select the tipster status'
					}
				}
			}				
		}		
	});   
   if ( $('#tipsAddForm').length ) {
	   $('#tipsAddForm')
	   .find('[name="colors"]')
	       .selectpicker()
	       .change(function(e) {
	           $('#tipsAddForm').bootstrapValidator('revalidateField', 'tipster_id');
	       })
	       .end()
	   .bootstrapValidator({
	       excluded: ':disabled',
	       feedbackIcons: {
	           valid: 'glyphicon glyphicon-ok',
	           invalid: 'glyphicon glyphicon-remove',
	           validating: 'glyphicon glyphicon-refresh'
	       },
	       fields: {
	    	   tipster_id: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please select the tipster.'
	                   }
	               }
	           },
	    	   date: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the date.'
	                   },date: {
							format: 'MM/DD/YYYY',
							message: 'The date should be in the MM/DD/YYYY format'
					   }
	               }
	           },
	    	   time: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the time.'
	                   }
	               }
	           },
	    	   fteam: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the first team.'
	                   }
	               }
	           },
	    	   steam: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the second team.'
	                   }
	               }
	           },
	    	   tip: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the tip.'
	                   }
	               }
	           },
	    	   odd: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the odd.'
	                   }
	               }
	           }           
	       }
	   });  
   }
   
   if ( $('#tipsEditForm').length ) {
	   $('#tipsEditForm')
	   .find('[name="colors"]')
	       .selectpicker()
	       .change(function(e) {
	           $('#tipsEditForm').bootstrapValidator('revalidateField', 'tipster_id');
	       })
	       .end()
	   .bootstrapValidator({
	       excluded: ':disabled',
	       feedbackIcons: {
	           valid: 'glyphicon glyphicon-ok',
	           invalid: 'glyphicon glyphicon-remove',
	           validating: 'glyphicon glyphicon-refresh'
	       },
	       fields: {
	    	   tipster_id: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please select the tipster.'
	                   }
	               }
	           },
	    	   date: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the date.'
	                   },date: {
							format: 'MM/DD/YYYY',
							message: 'The date should be in the MM/DD/YYYY format'
						},
	               }
	           },
	    	   time: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the time.'
	                   }
	               }
	           },
	    	   fteam: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the first team.'
	                   }
	               }
	           },
	    	   steam: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the second team.'
	                   }
	               }
	           },
	    	   tip: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the tip.'
	                   }
	               }
	           },
	    	   odd: {
	               validators: {
	                   notEmpty: {
	                       message: 'Please enter the odd.'
	                   }
	               }
	           }           
	       }
	   });  
   } 
   
   
   
   if ($('#announcementsAddForm').length) $('#announcement').ckeditor();
   if ($('#announcementsEditForm').length) $('#announcement').ckeditor();
   if ($('#adAddForm').length) $('#ad_content').ckeditor();
   if ($('#adEditForm').length) $('#ad_content').ckeditor();
   if ($('#cmsAddForm').length) $('#contents').ckeditor();
   if ($('#cmsEditForm').length) $('#contents').ckeditor();
   /*
   $('#announcementsAddForm').bootstrapValidator({
	   excluded: [':disabled'],     
       feedbackIcons: {
           valid: 'glyphicon glyphicon-ok',
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },
       fields: {
    	   announcement: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the announcement.'
                   },
			       callback: {
			           message: 'The announcement must be 200 characters long',
			           callback: function(value, validator, $field) {
			               // Get the plain text without HTML
			               var div  = $('<div/>').html(value).get(0),
			                   text = div.textContent || div.innerText;
			
			               return text.length <= 200;
			           }
			       }
               }
           },
    	   tip: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the tip.'
                   }
               }
           },
    	   odd: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the odd.'
                   }
               }
           }           
       }
   }).find('[name="announcement"]')
   	.ckeditor()
   	.editor
       // To use the 'change' event, use CKEditor 4.2 or later
       .on('change', function() {
           // Revalidate the bio field
           $('#announcementsAddForm').bootstrapValidator('revalidateField', 'announcement');
    });
       
   
   if ($('#announcementsEditForm').length){
   $('#announcementsEditForm').bootstrapValidator({
       feedbackIcons: {
           valid: 'glyphicon glyphicon-ok',
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },
       fields: {
    	   announcement: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the announcement.'
                   },
			       callback: {
			           message: 'The announcement must be 200 characters long',
			           callback: function(value, validator, $field) {
			               // Get the plain text without HTML
			               var div  = $('<div/>').html(value).get(0),
			                   text = div.textContent || div.innerText;
			
			               return text.length <= 200;
			           }
			       }
               }
           },
    	   tip: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the tip.'
                   }
               }
           },
    	   odd: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the odd.'
                   }
               }
           }           
       }
   }).find('[name="announcement"]')
   .ckeditor()
   .editor
       // To use the 'change' event, use CKEditor 4.2 or later
       .on('change', function() {
           // Revalidate the bio field
           $('#announcementsEditForm').bootstrapValidator('revalidateField', 'announcement');
   });
   
   }*/
   
   $('#doubleChanceTipsAddForm').bootstrapValidator({
       excluded: ':disabled',
       feedbackIcons: {
           valid: 'glyphicon glyphicon-ok',
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },
       fields: {
    	   tip: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the tip.'
                   }
               }
           },
    	   odd: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the odd.'
                   }
               }
           },
    	   time: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the time.'
                   }
               }
           },
    	   fteam: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the first team.'
                   }
               }
           },
    	   steam: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the second team.'
                   }
               }
           }
           
       }
   });     
   
   
   $('#doubleChanceTipsEditForm').bootstrapValidator({
       excluded: ':disabled',
       feedbackIcons: {
           valid: 'glyphicon glyphicon-ok',
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },
       fields: {
    	   tip: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the tip.'
                   }
               }
           },
    	   odd: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the odd.'
                   }
               }
           },
    	   time: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the time.'
                   }
               }
           },
    	   fteam: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the first team.'
                   }
               }
           },
    	   steam: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the second team.'
                   }
               }
           }
           
       }
   });   
   
   
   
   $('#planAddForm').bootstrapValidator({
       feedbackIcons: {
           valid: 'glyphicon glyphicon-ok',
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },
       fields: {
    	   plan_name: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the plan name.'
                   }
               }
           },
    	   plan_type: {
               validators: {
                   notEmpty: {
                       message: 'Please select the plan type.'
                   }
               }
           },
    	   amount: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the plan amount.'
                   },numeric :{
                	   message: 'Please enter a numeric value.'
                   }
               }
           },
    	   status: {
               validators: {
                   notEmpty: {
                       message: 'Please select the plan status.'
                   }
               }
           }
           
       }
   }); 
   
   $('#planEditForm').bootstrapValidator({
       excluded: ':disabled',
       feedbackIcons: {
           valid: 'glyphicon glyphicon-ok',
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },
       fields: {
    	   plan_name: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the plan name.'
                   }
               }
           },
    	   plan_type: {
               validators: {
                   notEmpty: {
                       message: 'Please select the plan type.'
                   }
               }
           },
    	   amount: {
               validators: {
                   notEmpty: {
                       message: 'Please enter the plan amount.'
                   },numeric :{
                	   message: 'Please enter a numeric value.'
                   }
               }
           },
    	   status: {
               validators: {
                   notEmpty: {
                       message: 'Please select the plan status.'
                   }
               }
           }
           
       }
   });   
   
   
});

/* 
 * 
 * Custom validation rules
 * 
 */
(function($) {
    $.fn.bootstrapValidator.validators.securePassword = {
        validate: function(validator, $field, options) {
            var value = $field.val();
            if (value === '') {
                return true;
            }

            // Check the password min length
            if (value.length < 8) {
                return {
                    valid: false,
                    message: 'The password must be more than 8 characters long'
                };
            }
            // Check the password max length
            if (value.length > 32) {
                return {
                    valid: false,
                    message: 'The password must be less than 32 characters'
                };
            }

            // The password doesn't contain any uppercase character
            if (value === value.toLowerCase()) {
                return {
                    valid: false,
                    message: 'The password must contain at least one upper case character'
                }
            }

            // The password doesn't contain any uppercase character
            if (value === value.toUpperCase()) {
                return {
                    valid: false,
                    message: 'The password must contain at least one lower case character'
                }
            }

            // The password doesn't contain any digit
            if (value.search(/[0-9]/) < 0) {
                return {
                    valid: false,
                    message: 'The password must contain at least one digit'
                }
            }

            return true;
        }
    };    
}(window.jQuery));

(function($) {
    $.fn.bootstrapValidator.validators.alphanumeric = {
        validate: function(validator, $field, options) {
            var value = $field.val();
           
             $('#user').on('input', function(evt) {                                                                       // Convert to uppercase
              $(this).val(function (_, val) {
                return val.toUpperCase();
              });
            })

            $('#user').bind("paste",function(e) {                                                                         // Disable Ctrl+V in User ID Field     
                  e.preventDefault();
            });
            
            $('#user').bind('keypress', function (event) {                                                                // Disable keys other than alpha numeric
                var regex = new RegExp("^[a-zA-Z0-9\b]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                   event.preventDefault();
                  }
            });
    
    
            if (value === '') {
                return true;
            }

            // Check the userid min length
            if (value.length < 6) {
                return {
                    valid: false,
                    message: 'The userid must be more than 5 characters long'
                };
            }
            // Check the userid max length
            if (value.length > 16) {
                return {
                    valid: false,
                    message: 'The userid must be less than 16 characters'
                };
            }

            // The userid doesn't contain any uppercase character
            if (value.search(/[A-Z]/) < 0) {
                return {
                    valid: false,
                    message: 'The userid must contain at least one alphabet'
                };
            }

            // The userid doesn't contain any digit
            if (value.search(/[0-9]/) < 0) {
                return {
                    valid: false,
                    message: 'The userid must contain at least one digit'
                }
            }

            return true;
        }
    };    
}(window.jQuery));


function CKEditorAnnounceValidation() {	   
	var editor_val = CKEDITOR.instances.announcement.document.getBody().getChild(0).getText() ;
    
	if ($.trim(editor_val) == '') {
		alert('Announcement cannot be empty!') ;
		return false ;
	}
    
	return true ;
}

function CKEditorAdsValidation() {	   
	var editor_val = CKEDITOR.instances.ad_content.document.getBody().getChild(0).getText() ;
    
	if ($.trim(editor_val) == '') {
		alert('Ad Content cannot be empty!') ;
		return false ;
	}
    
	return true ;
}

function CKEditorCmsValidation() {	   
	var editor_val = CKEDITOR.instances.contents.document.getBody().getChild(0).getText() ;
    
	if ($.trim(editor_val) == '') {
		alert('Content cannot be empty!') ;
		return false ;
	}
    
	return true ;
} 

	