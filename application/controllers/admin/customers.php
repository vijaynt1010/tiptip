<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
		$this->load->model('customersmodel');		
	}
	
	public function index(){
		$this->lists();	
	}
	
	public function lists() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Customers';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		
		$this->template_content['content']['customers'] = $this->customersmodel->get_customers();
		p($this->template_content['content']['customers']);die;
		//$this->template_content['content']['plan_types'] = c('plan_types');
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/customers/customers_lists',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);		
	}
	
	public function add(){
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Plans';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('plan_name', 'Plan Name', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('plan_type', 'Plan Type', 'trim|required|xss_clean');
			$this->form_validation->set_rules('amount', 'Plan Amount', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');			
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(
						'plan_name' => $this->input->post('plan_name'),
						'plan_type' => $this->input->post('plan_type'),
						'amount' => $this->input->post('amount'),
						'status' => $this->input->post('status'),
				);
				$status = $this->plansmodel->save_plan($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Plan saved successfully');
					redirect("admin/plans");
				} else {
					$this->session->set_flashdata('error_message','Plan not saved');
					redirect("admin/plans/add");
				}
			}
		}
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/plans/plans_add',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);	
	}
	
	public function edit($id = ''){
		if ( '' != $id ) {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Plans';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('plan_name', 'Plan Name', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('plan_type', 'Plan Type', 'trim|required|xss_clean');
			$this->form_validation->set_rules('amount', 'Plan Amount', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');	
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(
						'plan_id' => $id,
						'plan_name' => $this->input->post('plan_name'),
						'plan_type' => $this->input->post('plan_type'),
						'amount' => $this->input->post('amount'),
						'status' => $this->input->post('status'),
				);
				$status = $this->plansmodel->update_plan($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Plan updated successfully');
					redirect("admin/plans");
				} else {
					$this->session->set_flashdata('error_message','Plan not updated');
					redirect("admin/plans/edit/{$id}");
				}
			}
		}
		$this->template_content['content']['plan'] = $this->plansmodel->get_plan_by_id($id);
		if (!$this->template_content['content']['plan']){
			$this->session->set_flashdata('error_message','Plan not found');
			redirect("admin/plans");	
		}		
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/plans/plans_edit',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);
		} else{
			$this->session->set_flashdata('error_message','Plan not found');
			redirect("admin/plans");			
		}
	}

	public function delete($id = ''){
		if ($id != '') {
			if( $this->plansmodel->delete_plan($id)){
				$this->session->set_flashdata('success_message','Plan removed');
			}else{
				$this->session->set_flashdata('error_message','Plan not removed');
			}
		}
		redirect("admin/plans");		
	}
	
	public function changeStatus() {
		if ($this->plansmodel->update_plan_status()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}	
}