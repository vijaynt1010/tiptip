<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
	function __construct(){
		parent::__construct();
	}
	
	public function index(){		
		$this->loginmodel->logout();
		$this->session->set_flashdata('success_message','You have successfully logged out');
		redirect('admin/login');
	}
}