<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announcements extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
		$this->load->model('announcementsmodel');
	}
	
	public function index(){
		$this->announcements_lists();	
	}
	
	public function announcements_lists() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Announcements';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		
		$this->template_content['content']['announcements'] = $this->announcementsmodel->get_announcements();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/announcements/announcements_lists',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);		
	}
	
	public function add(){
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(				
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Announcements';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/ckeditor/ckeditor.js',
				'assets/backend/plugins/ckeditor/adapters/jquery.js',				
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ( $this->input->post() ) {			
			$this->form_validation->set_rules('announcement', 'Announcement', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('tip', 'Second Team', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('odd', 'Odd', 'trim|required|xss_clean');
			$this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');			
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(						
						'announcement' => $this->input->post('announcement'),
						'tip' => $this->input->post('tip'),
						'odd' => $this->input->post('odd'),
						'position' => $this->input->post('position'),
						'status' => $this->input->post('status'),						
				);
				$status = $this->announcementsmodel->save_announcement($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Announcement saved successfully');
					redirect("admin/announcements");
				} else {
					$this->session->set_flashdata('error_message','Announcement not saved');
					redirect("admin/announcements/add");
				}
			}
		}		
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/announcements/announcements_add',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);	
	}
	
	public function edit($id = ''){
		if ( '' != $id ) {
			$this->template_content['header'] = array();
			$this->template_content['content'] =  array();
			$this->template_content['footer'] = array();
			$this->template_content['header']['_cssFiles'] = array(
					'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
			);
			$this->template_content['header']['_title'] = ' :: Announcements';
			$this->template_content['footer']['_jsFiles'] = array(
					'assets/backend/plugins/ckeditor/ckeditor.js',
					'assets/backend/plugins/ckeditor/adapters/jquery.js',					
					'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
					'assets/backend/js/customs/validation/validation.js',
					'assets/backend/js/jquery.dataTables.min.js'
			);
			if ($this->input->post()) {
				$this->form_validation->set_rules('announcement', 'Announcement', 'trim|required|xss_clean');
				//$this->form_validation->set_rules('tip', 'Second Team', 'trim|required|xss_clean');
				//$this->form_validation->set_rules('odd', 'Odd', 'trim|required|xss_clean');
				$this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
				$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');	
				if ($this->form_validation->run() == FALSE) {
					$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
				} else {
					$data_array = array(
							'announce_id' => $id,
							'announcement' => $this->input->post('announcement'),
							'tip' => $this->input->post('tip'),
							'odd' => $this->input->post('odd'),
							'position' => $this->input->post('position'),
							'status' => $this->input->post('status')
					);
					$status = $this->announcementsmodel->update_announcement($data_array);
					if($status == true){
						$this->session->set_flashdata('success_message','Announcement updated successfully');
						redirect("admin/announcements");
					} else {
						$this->session->set_flashdata('error_message','Announcement not updated');
						redirect("admin/announcements/edit/{$id}");
					}
				}
			}			
			$this->template_content['content']['announcement'] = $this->announcementsmodel->get_announcement_by_id($id);
			if (!$this->template_content['content']['announcement']){
				$this->session->set_flashdata('error_message','Announcement not found');
				redirect("admin/announcements");	
			}		
			$this->load->view('admin/common/header',$this->template_content['header']);
			$this->load->view('admin/announcements/announcements_edit',$this->template_content['content']);
			$this->load->view('admin/common/footer',$this->template_content['footer']);
		} else{
			$this->session->set_flashdata('error_message','Announcement not found');
			redirect("admin/announcements");			
		}
	}

	public function delete($id = ''){
		if ($id != '') {
			if( $this->announcementsmodel->delete_announcement($id)){
				$this->session->set_flashdata('success_message','Announcement removed');
			}else{
				$this->session->set_flashdata('error_message','Announcement not removed');
			}
		}
		redirect("admin/announcements");		
	}
	
	public function changePosition() {
		if ($this->announcementsmodel->update_announcement_position()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}
	public function changeStatus() {
		if ($this->announcementsmodel->update_announcement_status()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}
}