<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doublechancetips extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
		$this->load->model('doublechancetipsmodel');
	}
	
	public function index(){
		$this->lists();	
	}
	
	public function lists() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Double Chance Tips';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		
		$this->template_content['content']['tips'] = $this->doublechancetipsmodel->get_tips();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/doublechancetips/lists',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);		
	}
	
	public function add(){
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Double Chance Tips';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ( $this->input->post() ) {
			$this->form_validation->set_rules('tip', 'Tip', 'trim|required|xss_clean');
			$this->form_validation->set_rules('odd', 'Odd', 'trim|required|xss_clean');
			$this->form_validation->set_rules('time', 'Time', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fteam', 'First Team', 'trim|required|xss_clean');
			$this->form_validation->set_rules('steam', 'Second Team', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');			
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(
						'tip' => $this->input->post('tip'),
						'odd' => $this->input->post('odd'),						
						'time' => $this->input->post('time'),
						'fteam' => $this->input->post('fteam'),
						'steam' => $this->input->post('steam'),		
						'row_onef1' => $this->input->post('row_onef1'),
						'row_onef2' => $this->input->post('row_onef2'),
						'row_onef3' => $this->input->post('row_onef3'),
						'row_twof1' => $this->input->post('row_twof1'),
						'row_twof2' => $this->input->post('row_twof2'),
						'row_twof3' => $this->input->post('row_twof3'),		
						'btm_column' => $this->input->post('btm_column'),					
						'status' => $this->input->post('status'),						
				);
				$status = $this->doublechancetipsmodel->save_tip($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Tip saved successfully');
					redirect("admin/doublechancetips");
				} else {
					$this->session->set_flashdata('error_message','Tip not saved');
					redirect("admin/doublechancetips/add");
				}
			}
		}		
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/doublechancetips/tips_add',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);	
	}
	
	public function edit($id = ''){
		if ( '' != $id ) {
			$this->template_content['header'] = array();
			$this->template_content['content'] =  array();
			$this->template_content['footer'] = array();
			$this->template_content['header']['_cssFiles'] = array(
					'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
			);
			$this->template_content['header']['_title'] = ' :: Tips';
			$this->template_content['footer']['_jsFiles'] = array(
					'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
					'assets/backend/js/customs/validation/validation.js',
					'assets/backend/js/jquery.dataTables.min.js'
			);
			if ($this->input->post()) {
			$this->form_validation->set_rules('tip', 'Tip', 'trim|required|xss_clean');
			$this->form_validation->set_rules('odd', 'Odd', 'trim|required|xss_clean');
			$this->form_validation->set_rules('time', 'Time', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fteam', 'First Team', 'trim|required|xss_clean');
			$this->form_validation->set_rules('steam', 'Second Team', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
				if ($this->form_validation->run() == FALSE) {
					$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
				} else {
					$data_array = array(
						'id' => $id,
						'tip' => $this->input->post('tip'),
						'odd' => $this->input->post('odd'),						
						'time' => $this->input->post('time'),
						'fteam' => $this->input->post('fteam'),
						'steam' => $this->input->post('steam'),		
						'row_onef1' => $this->input->post('row_onef1'),
						'row_onef2' => $this->input->post('row_onef2'),
						'row_onef3' => $this->input->post('row_onef3'),
						'row_twof1' => $this->input->post('row_twof1'),
						'row_twof2' => $this->input->post('row_twof2'),
						'row_twof3' => $this->input->post('row_twof3'),		
						'btm_column' => $this->input->post('btm_column'),					
						'status' => $this->input->post('status'),	
					);
					$status = $this->doublechancetipsmodel->update_tips($data_array);
					if($status == true){
						$this->session->set_flashdata('success_message','Tips updated successfully');
						redirect("admin/doublechancetips");
					} else {
						$this->session->set_flashdata('error_message','Tips not updated');
						redirect("admin/doublechancetips/edit/{$id}");
					}
				}
			}
			//$this->template_content['content']['tipsters'] = $this->tipsmodel->get_active_tipsters();
			$this->template_content['content']['tip'] = $this->doublechancetipsmodel->get_tip_by_id($id);
			if (!$this->template_content['content']['tip']){
				$this->session->set_flashdata('error_message','Tips not found');
				redirect("admin/doublechancetips");	
			}		
			$this->load->view('admin/common/header',$this->template_content['header']);
			$this->load->view('admin/doublechancetips/tips_edit',$this->template_content['content']);
			$this->load->view('admin/common/footer',$this->template_content['footer']);
		} else{
			$this->session->set_flashdata('error_message','Tips not found');
			redirect("admin/doublechancetips");			
		}
	}

	public function delete($id = ''){
		if ($id != '') {
			if( $this->doublechancetipsmodel->delete_tip($id)){
				$this->session->set_flashdata('success_message','Tip removed');
			}else{
				$this->session->set_flashdata('error_message','Tip not removed');
			}
		}
		redirect("admin/doublechancetips");		
	}
	
	public function changeStatus() {
		if ($this->doublechancetipsmodel->update_tips_status()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}		
}