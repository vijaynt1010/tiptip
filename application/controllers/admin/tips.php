<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tips extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
		$this->load->model('tipsmodel');
	}
	
	public function index(){
		$this->tips_lists();	
	}
	
	public function tips_lists() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Tips';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		
		$this->template_content['content']['tips'] = $this->tipsmodel->get_tips();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/tips/tips_lists',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);		
	}
	
	public function left() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Tips';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
	
		$this->template_content['content']['tips'] = $this->tipsmodel->get_tips_left();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/tips/tips_lists_left',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);
	}	
	
	public function right() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Tips';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
	
		$this->template_content['content']['tips'] = $this->tipsmodel->get_tips_right();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/tips/tips_lists_right',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);
	}	
	
	public function archives() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Tips';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
	
		$this->template_content['content']['tips'] = $this->tipsmodel->get_tips_archives();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/tips/tips_lists_archives',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);
	}	
	
	public function add(){
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapselect/dist/css/bootstrap-select.min.css',
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Tips';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapselect/dist/js/bootstrap-select.min.js',
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ( $this->input->post() ) {
			$this->form_validation->set_rules('tipster_id', 'Tipster Name', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
			$this->form_validation->set_rules('time', 'Time', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fteam', 'First Team', 'trim|required|xss_clean');
			$this->form_validation->set_rules('steam', 'Second Team', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tip', 'Second Team', 'trim|required|xss_clean');
			$this->form_validation->set_rules('odd', 'Odd', 'trim|required|xss_clean');
			$this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
			$this->form_validation->set_rules('publish', 'Publish', 'trim|required|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(
						'tipster_id' => $this->input->post('tipster_id'),
						'date' => $this->input->post('date'),
						'time' => $this->input->post('time'),
						'fteam' => $this->input->post('fteam'),
						'steam' => $this->input->post('steam'),
						'tip' => $this->input->post('tip'),
						'odd' => $this->input->post('odd'),
						'position' => $this->input->post('position'),
						'status' => $this->input->post('status'),
						'publish' => $this->input->post('publish'),
				);
				$status = $this->tipsmodel->save_tip($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Tip saved successfully');
					redirect("admin/tips");
				} else {
					$this->session->set_flashdata('error_message','Tip not saved');
					redirect("admin/tipster_add");
				}
			}
		}
		$this->template_content['content']['tipsters'] = $this->tipsmodel->get_active_tipsters();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/tips/tips_add',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);	
	}
	
	public function edit($id = ''){
		if ( '' != $id ) {
			$this->template_content['header'] = array();
			$this->template_content['content'] =  array();
			$this->template_content['footer'] = array();
			$this->template_content['header']['_cssFiles'] = array(
					'assets/backend/plugins/bootstrapselect/dist/css/bootstrap-select.min.css',
					'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
			);
			$this->template_content['header']['_title'] = ' :: Tips';
			$this->template_content['footer']['_jsFiles'] = array(
					'assets/backend/plugins/bootstrapselect/dist/js/bootstrap-select.min.js',
					'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
					'assets/backend/js/customs/validation/validation.js',
					'assets/backend/js/jquery.dataTables.min.js'
			);
			if ($this->input->post()) {
				$this->form_validation->set_rules('tipster_id', 'Tipster Name', 'trim|required|xss_clean');			
				$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
				$this->form_validation->set_rules('time', 'Time', 'trim|required|xss_clean');
				$this->form_validation->set_rules('fteam', 'First Team', 'trim|required|xss_clean');
				$this->form_validation->set_rules('steam', 'Second Team', 'trim|required|xss_clean');
				$this->form_validation->set_rules('tip', 'Second Team', 'trim|required|xss_clean');
				$this->form_validation->set_rules('odd', 'Odd', 'trim|required|xss_clean');
				$this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
				$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
				$this->form_validation->set_rules('publish', 'Publish', 'trim|required|xss_clean');
				if ($this->form_validation->run() == FALSE) {
					$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
				} else {
					$data_array = array(
							'tipid' => $id,
							'tipster_id' => $this->input->post('tipster_id'),
							'date' => $this->input->post('date'),
							'time' => $this->input->post('time'),
							'fteam' => $this->input->post('fteam'),
							'steam' => $this->input->post('steam'),
							'tip' => $this->input->post('tip'),
							'odd' => $this->input->post('odd'),
							'position' => $this->input->post('position'),
							'status' => $this->input->post('status'),
							'publish' => $this->input->post('publish'),
					);
					$status = $this->tipsmodel->update_tips($data_array);
					if($status == true){
						$this->session->set_flashdata('success_message','Tips updated successfully');
						redirect("admin/tips");
					} else {
						$this->session->set_flashdata('error_message','Tips not updated');
						redirect("admin/tips/edit/{$id}");
					}
				}
			}
			$this->template_content['content']['tipsters'] = $this->tipsmodel->get_active_tipsters();
			$this->template_content['content']['tip'] = $this->tipsmodel->get_tip_by_id($id);
			if (!$this->template_content['content']['tip']){
				$this->session->set_flashdata('error_message','Tips not found');
				redirect("admin/tips");	
			}		
			$this->load->view('admin/common/header',$this->template_content['header']);
			$this->load->view('admin/tips/tips_edit',$this->template_content['content']);
			$this->load->view('admin/common/footer',$this->template_content['footer']);
		} else{
			$this->session->set_flashdata('error_message','Tips not found');
			redirect("admin/tips");			
		}
	}

	public function delete($id = ''){
		if ($id != '') {
			if( $this->tipsmodel->delete_tip($id)){
				$this->session->set_flashdata('success_message','Tip removed');
			}else{
				$this->session->set_flashdata('error_message','Tip not removed');
			}
		}
		redirect("admin/tips");		
	}
	
	public function changePublishStatus() {
		if ($this->tipsmodel->update_tip_publish_status()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}	
	public function changePosition() {
		if ($this->tipsmodel->update_tip_position()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}	
}