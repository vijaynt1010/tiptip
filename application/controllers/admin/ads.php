<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ads extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
		$this->load->model('adsmodel');
	}
	
	public function index(){
		$this->lists();	
	}
	
	public function lists() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Ads';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		
		$this->template_content['content']['ads'] = $this->adsmodel->get_ads();
		$this->template_content['content']['ads_positions'] = c('ads_position');
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/ads/ads_lists',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);		
	}
	
	public function add(){
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(				
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Ads';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/ckeditor/ckeditor.js',
				'assets/backend/plugins/ckeditor/adapters/jquery.js',				
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ( $this->input->post() ) {			
			$this->form_validation->set_rules('ad_content', 'Ad Content', 'trim|required|xss_clean');
			$this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');			
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(						
						'content' => $this->input->post('ad_content'),
						'position' => $this->input->post('position'),
						'status' => $this->input->post('status'),						
				);
				$status = $this->adsmodel->save_ad($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Ad saved successfully');
					redirect("admin/ads");
				} else {
					$this->session->set_flashdata('error_message','Ad not saved');
					redirect("admin/ads/add");
				}
			}
		}		
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/ads/ads_add',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);	
	}
	
	public function edit($id = ''){
		if ( '' != $id ) {
			$this->template_content['header'] = array();
			$this->template_content['content'] =  array();
			$this->template_content['footer'] = array();
			$this->template_content['header']['_cssFiles'] = array(
					'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
			);
			$this->template_content['header']['_title'] = ' :: Ads';
			$this->template_content['footer']['_jsFiles'] = array(
					'assets/backend/plugins/ckeditor/ckeditor.js',
					'assets/backend/plugins/ckeditor/adapters/jquery.js',					
					'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
					'assets/backend/js/customs/validation/validation.js',
					'assets/backend/js/jquery.dataTables.min.js'
			);
			if ($this->input->post()) {
			$this->form_validation->set_rules('ad_content', 'Ad Content', 'trim|required|xss_clean');
			$this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');		
				if ($this->form_validation->run() == FALSE) {
					$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
				} else {
					$data_array = array(
						'ad_id' => $id,
						'content' => $this->input->post('ad_content'),
						'position' => $this->input->post('position'),
						'status' => $this->input->post('status'),	
					);
					$status = $this->adsmodel->update_ad($data_array);
					if($status == true){
						$this->session->set_flashdata('success_message','Ad updated successfully');
						redirect("admin/ads");
					} else {
						$this->session->set_flashdata('error_message','Ad not updated');
						redirect("admin/ads/edit/{$id}");
					}
				}
			}			
			$this->template_content['content']['ads'] = $this->adsmodel->get_ad_by_id($id);
			if (!$this->template_content['content']['ads']){
				$this->session->set_flashdata('error_message','Ad not found');
				redirect("admin/ads");	
			}		
			$this->load->view('admin/common/header',$this->template_content['header']);
			$this->load->view('admin/ads/ads_edit',$this->template_content['content']);
			$this->load->view('admin/common/footer',$this->template_content['footer']);
		} else{
			$this->session->set_flashdata('error_message','Ad not found');
			redirect("admin/ads");			
		}
	}

	public function delete($id = ''){
		if ($id != '') {
			if( $this->adsmodel->delete_ad($id)){
				$this->session->set_flashdata('success_message','Ad removed');
			}else{
				$this->session->set_flashdata('error_message','Ad not removed');
			}
		}
		redirect("admin/ads");		
	}
	
	public function changePosition() {
		if ($this->announcementsmodel->update_announcement_position()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}
	public function changeStatus() {
		if ($this->adsmodel->update_ad_status()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}
}