<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
	}
	public function index()
	{
		$this->template_content['header']['_title'] = ' :: Admin Dashboard';
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/dashboard',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);

	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */