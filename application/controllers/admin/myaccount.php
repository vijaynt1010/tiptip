<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myaccount extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
	}
	
	public function changepassword(){
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();		
		$this->template_content['header']['_cssFiles'] = array(			
			'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',	
		);
		$this->template_content['header']['_title'] = ' :: Plans';	
		$this->template_content['footer']['_jsFiles'] = array(
			'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
			'assets/backend/js/customs/validation/validation.js',
			'assets/backend/js/jquery.dataTables.min.js'
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('current_password', 'Current Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|max_length[32]|min_length[8]|xss_clean');
			$this->form_validation->set_rules('confirm_newpassword', 'Confirm New Password', 'trim|required|max_length[32]|min_length[8]|matches[new_password]|xss_clean');			
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {	
				$password_data = array(
					'current_password' => $this->input->post('current_password'),
					'new_password' => $this->input->post('new_password')
				);
				if($this->loginmodel->changepassword($password_data)){
					$this->session->set_flashdata('success_message','Password changed successfully');
				}else{
					$this->session->set_flashdata('error_message','Password change failed');
				}
				redirect('admin/myaccount/changepassword');
			}		
		}		
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/myaccount/changepassword',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);		
	}
	
	public function profile() {
		 
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Profile';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		
		$this->template_content['content']['profile'] = $this->loginmodel->getProfileOfLoggedUser();		
		if ($this->input->post()) {
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('user_name', 'User Name', 'trim|required|is_unique[admin_users.user_name.id.'.$this->template_content['content']['profile']->id.']|xss_clean');
			$this->form_validation->set_rules('email_id', 'Email', 'trim|required|valid_email|is_unique[admin_users.email_id.id.'.$this->template_content['content']['profile']->id.']|xss_clean');
		
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$profile_data = array(
					'id' => $this->template_content['content']['profile']->id,
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'email_id' => $this->input->post('email_id'),
					'user_name' => $this->input->post('user_name'),
				);
				if ( $this->loginmodel->updateProfile($profile_data) ) {
					$this->session->set_flashdata('success_message','Profile updated successfully.');
				} else {
					$this->session->set_flashdata('error_message','Profile updation failed.');
				}
				redirect('admin/myaccount/profile');
			}
		}
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/myaccount/profile',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);			
	}

	public function is_username_available(){
		$user_name_check =  $this->input->get('user_name',TRUE);		
		if (!$this->loginmodel->is_username_available($user_name_check)) {
			echo json_encode(array(
				'valid' => true
			));exit;
		}else{
			echo json_encode(array(
					'valid' => false
			));exit;			
		}
	}
	public function is_email_available(){
		$email_id_check =  $this->input->get('email_id',TRUE);
		if (!$this->loginmodel->is_email_available($email_id_check)) {
			echo json_encode(array(
					'valid' => true
			));exit;
		}else{
			echo json_encode(array(
					'valid' => false
			));exit;
		}
	}	
}