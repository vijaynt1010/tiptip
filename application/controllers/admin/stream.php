<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stream extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
		$this->load->model('streammodel');
	}
	
	public function index(){
		$this->stream_lists();	
	}
	
	public function stream_lists() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Stream';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		
		$this->template_content['content']['streams'] = $this->streammodel->get_streams();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/stream/stream_lists',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);		
	}
	
	public function add(){
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Stream';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('stream_code', 'Stream Code', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(
						'stream_code' => $this->input->post('stream_code'),
						'status' => $this->input->post('status')
				);
				$status = $this->streammodel->save_stream($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Stream saved successfully');
					redirect("admin/stream");
				} else {
					$this->session->set_flashdata('error_message','Stream not saved');
					redirect("admin/stream/add");
				}
			}
		}
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/stream/stream_add',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);	
	}
	
	public function edit($id = ''){
		if ( '' != $id ) {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Stream';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('stream_code', 'Stream Code', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(
						'id' => $id,
						'stream_code' => $this->input->post('stream_code'),
						'status' => $this->input->post('status')
				);
				$status = $this->streammodel->update_stream($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Stream updated successfully');
					redirect("admin/stream");
				} else {
					$this->session->set_flashdata('error_message','Stream not updated');
					redirect("admin/stream/edit/{$id}");
				}
			}
		}
		$this->template_content['content']['stream'] = $this->streammodel->get_stream_by_id($id);
		if (!$this->template_content['content']['stream']){
			$this->session->set_flashdata('error_message','Stream not found');
			redirect("admin/stream");	
		}		
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/stream/stream_edit',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);
		} else{
			$this->session->set_flashdata('error_message','Stream not found');
			redirect("admin/stream");			
		}
	}

	public function delete($id = ''){
		if ($id != '') {
			if( $this->streammodel->delete_stream($id)){
				$this->session->set_flashdata('success_message','Stream removed');
			}else{
				$this->session->set_flashdata('error_message','Stream not removed');
			}
		}
		redirect("admin/stream");		
	}
	
	public function changeStatus() {
		if ($this->streammodel->update_stream_status()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}	
}