<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		//$t_hasher = new PasswordHash(8, FALSE);
		
		//$correct = 'Test12345';
		//$hash = $t_hasher->HashPassword($correct);die($hash);
		if ($this->loginmodel->is_logged_in()) {
			redirect('admin/dashboard');
		}
		$this->gen_contents = array();
	}
	public function index()
	{
		if ( $this->session->flashdata('success_message') ){
			$this->gen_contents['success_message'] = $this->session->flashdata('success_message');
		}
		if ( $this->session->flashdata('error_message') ){
			$this->gen_contents['error_message'] = $this->session->flashdata('error_message');
		}
		if ( $this->session->flashdata('forgot_success_message') ){
			$this->gen_contents['forgot_success_message'] = $this->session->flashdata('forgot_success_message');
		}
		if ( $this->session->flashdata('forgot_error_message') ){
			$this->gen_contents['forgot_error_message'] = $this->session->flashdata('forgot_error_message');
		}
		if ( $this->session->flashdata('form_action') ){
			$this->gen_contents['form_action'] = $this->session->flashdata('form_action');
		}		
			
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_password', 'Password', 'trim|required|xss_clean');			
		if ($this->form_validation->run() == FALSE) {
			
			$this->load->view('admin/login',$this->gen_contents);
			
		} else {
			$login_data = array(
				'user_name' => $this->input->post('user_name'),
				'user_password' => $this->input->post('user_password'),
			);
			if ($this->loginmodel->process_login($login_data)) {
				redirect('admin/dashboard');
			} else {
				$this->session->set_flashdata('error_message','Invalid User Name or Password');
				redirect('admin/login');
			}			
		}			

	}

	
	public function forgotpassword() {

		$this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('forgot_error_message','Please enter a valid email id');
			$this->session->set_flashdata('form_action','forgotpassword');
			redirect('admin/login');
		} else {
			$login = array("user_email" => $this->input->post("user_email"));
			if ($this->loginmodel->doForgotPassword($login)) {
				$this->session->set_flashdata('forgot_success_message','An email has sent to your email id.');
				$this->session->set_flashdata('form_action','forgotpassword');
				redirect('admin/login');
			}else{
				$this->session->set_flashdata('forgot_error_message','No matching user is found for the entered email id');
				$this->session->set_flashdata('form_action','forgotpassword');
				redirect('admin/login');
			}
		}

	}
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */