<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reset extends CI_Controller {
    
    var $gen_contents = array();
    public function __construct() {
        parent::__construct();
    }   
    
    public function password($resetKey = NULL , $userId = NULL){
        
            $this->gen_contents['reset_key'] = $resetKey;
            $this->gen_contents['user_id'] = $userId;
            if ($this->loginmodel->isValidResetKey($resetKey, $userId)){            
                $this->form_validation->set_rules('user_newpassword', 'New Password', 'trim|required|matches[user_confirmpassword]|min_length[8]|max_length[32]|xss_clean');
                $this->form_validation->set_rules('user_confirmpassword', 'Retype Password', 'trim|required|min_length[8]|max_length[32]|xss_clean');

                if ($this->form_validation->run() == FALSE) {; 
                    $this->load->view('admin/reset_password.php',$this->gen_contents);
                } else {

                      $login = array("password_reset_key"=>$resetKey,"user_id"=>$userId,"user_newpassword"=>$this->input->post('user_newpassword'));
                      if($this->loginmodel->doResetPassword($login)){
                          $this->session->set_flashdata('success_message', 'Password reset is success');
                          redirect('admin/login');
                      }else{
                        $this->gen_contents['error_message'] = 'An unknown error occured!';  
                        $this->load->view('admin/reset_password_error.php',$this->gen_contents);
                      }
                } 
            }else{      
                $this->gen_contents['error_message'] = 'Invalid password reset link'; 
                $this->load->view('admin/reset_password_error.php',$this->gen_contents);
            }            
    }
    
}