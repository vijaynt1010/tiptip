<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipster extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
		$this->load->model('tipstermodel');
	}
	
	public function index(){
		$this->tipster_lists();	
	}
	
	public function tipster_lists() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Tipster';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		
		$this->template_content['content']['tipsters'] = $this->tipstermodel->get_tipsters();
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/tipster/tipster_lists',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);		
	}
	
	public function add(){
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Tipster';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('tipster_name', 'Tipster Name', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('tipster_status', 'Tipster Status', 'trim|required|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(
						'tipster_name' => $this->input->post('tipster_name'),
						'tipster_status' => $this->input->post('tipster_status')
				);
				$status = $this->tipstermodel->save_tipster($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Tipster saved successfully');
					redirect("admin/tipster");
				} else {
					$this->session->set_flashdata('error_message','Tipster not saved');
					redirect("admin/tipster_add");
				}
			}
		}
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/tipster/tipster_add',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);	
	}
	
	public function edit($id = ''){
		if ( '' != $id ) {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Services';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('tipster_name', 'Tipster Name', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('tipster_status', 'Tipster Status', 'trim|required|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(
						'id' => $id,
						'tipster_name' => $this->input->post('tipster_name'),
						'tipster_status' => $this->input->post('tipster_status')
				);
				$status = $this->tipstermodel->update_tipster($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Tipster updated successfully');
					redirect("admin/tipster");
				} else {
					$this->session->set_flashdata('error_message','Tipster not updated');
					redirect("admin/tipster/edit/{$id}");
				}
			}
		}
		$this->template_content['content']['tipster'] = $this->tipstermodel->get_tipster_by_id($id);
		if (!$this->template_content['content']['tipster']){
			$this->session->set_flashdata('error_message','Tipster not found');
			redirect("admin/tipster");	
		}		
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/tipster/tipster_edit',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);
		} else{
			$this->session->set_flashdata('error_message','Tipster not found');
			redirect("admin/tipster");			
		}
	}

	public function delete($id = ''){
		if ($id != '') {
			if( $this->tipstermodel->delete_tipster($id)){
				$this->session->set_flashdata('success_message','Tipster removed');
			}else{
				$this->session->set_flashdata('error_message','Tipster not removed');
			}
		}
		redirect("admin/tipster");		
	}
	
	public function changeStatus() {
		if ($this->tipstermodel->update_tipster_status()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}	
}