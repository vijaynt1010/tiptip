<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller {
	function __construct(){
		parent::__construct();
		if (!$this->loginmodel->is_logged_in()) {
			redirect('admin/login');
		}
		$this->template_content = array();
		$this->load->model('cmsmodel');
		/*$this->db->query("CREATE TABLE `cms` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `content` text NOT NULL,
 `position` int(11) NOT NULL COMMENT '1 = top, 2 = right, 3 = bottom, 4 = left, 5 = middle',
 `status` int(11) NOT NULL COMMENT '0 = incative, 1 = active',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1");

ALTER TABLE `plans` ADD `status` INT NOT NULL COMMENT '1 = active, 0 = inactive' ;
ALTER TABLE `plans` CHANGE `plan_id` `id` INT(11) NOT NULL AUTO_INCREMENT;
die;*/
	}
	
	public function index(){
		$this->lists();	
	}
	
	public function lists() {
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css',
		);
		$this->template_content['header']['_title'] = ' :: Website Content';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		
		$this->template_content['content']['cms'] = $this->cmsmodel->get_cms();
		$this->template_content['content']['cms_positions'] = c('cms_position');
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/cms/cms_lists',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);		
	}
	
	public function add(){
		$this->template_content['header'] = array();
		$this->template_content['content'] =  array();
		$this->template_content['footer'] = array();
		$this->template_content['header']['_cssFiles'] = array(				
				'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
		);
		$this->template_content['header']['_title'] = ' :: Website Content';
		$this->template_content['footer']['_jsFiles'] = array(
				'assets/backend/plugins/ckeditor/ckeditor.js',
				'assets/backend/plugins/ckeditor/adapters/jquery.js',				
				'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
				'assets/backend/js/customs/validation/validation.js',
				'assets/backend/js/jquery.dataTables.min.js'
		);
		if ( $this->input->post() ) {						
			$this->form_validation->set_rules('content', 'Content', 'trim|required|xss_clean');
			$this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
			} else {
				$data_array = array(							
						'content' => $this->input->post('content'),
						'position' => $this->input->post('position'),
						'status' => $this->input->post('status'),						
				);
				$status = $this->cmsmodel->save_Cms($data_array);
				if($status == true){
					$this->session->set_flashdata('success_message','Website Content saved successfully');
					redirect("admin/cms");
				} else {
					$this->session->set_flashdata('error_message','Website Content not saved');
					redirect("admin/cms/add");
				}
			}
		}		
		$this->load->view('admin/common/header',$this->template_content['header']);
		$this->load->view('admin/cms/cms_add',$this->template_content['content']);
		$this->load->view('admin/common/footer',$this->template_content['footer']);	
	}
	
	public function edit($id = ''){
		if ( '' != $id ) {
			$this->template_content['header'] = array();
			$this->template_content['content'] =  array();
			$this->template_content['footer'] = array();
			$this->template_content['header']['_cssFiles'] = array(
					'assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css'
			);
			$this->template_content['header']['_title'] = ' :: Website Content';
			$this->template_content['footer']['_jsFiles'] = array(
					'assets/backend/plugins/ckeditor/ckeditor.js',
					'assets/backend/plugins/ckeditor/adapters/jquery.js',					
					'assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js',
					'assets/backend/js/customs/validation/validation.js',
					'assets/backend/js/jquery.dataTables.min.js'
			);
			if ($this->input->post()) {
			$this->form_validation->set_rules('content', 'Content', 'trim|required|xss_clean');
			$this->form_validation->set_rules('position', 'Position', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');		
				if ($this->form_validation->run() == FALSE) {
					$this->template_content['content']['error_message'] =  validation_errors('<p>', '</p>');
				} else {
					$data_array = array(
						'id' => $id,
						'content' => $this->input->post('content'),
						'position' => $this->input->post('position'),
						'status' => $this->input->post('status'),		
					);
					$status = $this->cmsmodel->update_cms($data_array);
					if($status == true){
						$this->session->set_flashdata('success_message','Website content updated successfully');
						redirect("admin/cms");
					} else {
						$this->session->set_flashdata('error_message','Website content not updated');
						redirect("admin/cms/edit/{$id}");
					}
				}
			}			
			$this->template_content['content']['cms'] = $this->cmsmodel->get_cms_by_id($id);
			if (!$this->template_content['content']['cms']){
				$this->session->set_flashdata('error_message','Website content not found');
				redirect("admin/cms");	
			}		
			$this->load->view('admin/common/header',$this->template_content['header']);
			$this->load->view('admin/cms/cms_edit',$this->template_content['content']);
			$this->load->view('admin/common/footer',$this->template_content['footer']);
		} else{
			$this->session->set_flashdata('error_message','Website content not found');
			redirect("admin/cms");			
		}
	}

	public function delete($id = ''){
		if ($id != '') {
			if( $this->cmsmodel->delete_cms($id)){
				$this->session->set_flashdata('success_message','Website content removed');
			}else{
				$this->session->set_flashdata('error_message','Website content not removed');
			}
		}
		redirect("admin/cms");		
	}
	

	public function changeStatus() {
		if ($this->cmsmodel->update_cms_status()) {
			$response['status'] = true;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);exit;
	}
}