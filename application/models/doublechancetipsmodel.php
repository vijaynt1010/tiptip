<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Doublechancetipsmodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_tips(){
		//$this->db->order_by('announcements.announcement','ASC');				
		return  $this->db->get('double_chance_tips')->result();
	}	
	public function save_tip($data_array){
		if (is_array($data_array) && count($data_array) > 0 ) {
			if($this->db->insert('double_chance_tips',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function get_tip_by_id($id = ''){
		if ($id != '') {
			$this->db->where('id',$id);
			return  $this->db->get('double_chance_tips')->row();
		}
		return false;		
	}
	
	public function update_tips($data_array = array()){
		if (is_array($data_array) && count($data_array) > 0 ) {
			$this->db->where('id',$data_array['id']);
			if($this->db->update('double_chance_tips',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function delete_tip($id=''){
		if ($id != '') {
			$this->db->where('id',$id);
			return  $this->db->delete('double_chance_tips');
		}
		return false;
	}
	public function update_tips_status(){
		$this->db->where('id',$this->input->post('tip_id',true));
		return $this->db->update('double_chance_tips',array('status' => $this->input->post('status',true)));
	}
}