<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tipsmodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_tips(){
		$this->db->order_by('tips.date','ASC');
		$this->db->join('tipster','tipster.id=tips.tipster_id');
		$this->db->select('tips.*,tipster.tipster_name');
		return  $this->db->get('tips')->result();
	}
	public function get_tips_left(){
		$this->db->where('tips.position','0');
		$this->db->where('tips.status','0');
		$this->db->order_by('tips.date','ASC');
		$this->db->join('tipster','tipster.id=tips.tipster_id');
		$this->db->select('tips.*,tipster.tipster_name');
		return  $this->db->get('tips')->result();
	}
	public function get_tips_right(){
		$this->db->where('tips.position','1');
		$this->db->where('tips.status','0');
		$this->db->order_by('tips.date','ASC');
		$this->db->join('tipster','tipster.id=tips.tipster_id');
		$this->db->select('tips.*,tipster.tipster_name');
		return  $this->db->get('tips')->result();
	}
	public function get_tips_archives(){
		$this->db->where_in('tips.status',array('1','2'));
		$this->db->order_by('tips.date','ASC');
		$this->db->join('tipster','tipster.id=tips.tipster_id');
		$this->db->select('tips.*,tipster.tipster_name');
		return  $this->db->get('tips')->result();
	}	
	public function get_active_tipsters(){
		$this->db->where('tipster_status',1);
		$this->db->order_by('tipster_name','ASC');
		return  $this->db->get('tipster')->result();
	}	
	public function save_tip($data_array){
		if (is_array($data_array) && count($data_array) > 0 ) {
			if($this->db->insert('tips',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function get_tip_by_id($id = ''){
		if ($id != '') {
			$this->db->where('tipid',$id);
			return  $this->db->get('tips')->row();
		}
		return false;		
	}
	
	public function update_tips($data_array = array()){
		if (is_array($data_array) && count($data_array) > 0 ) {
			$this->db->where('tipid',$data_array['tipid']);
			if($this->db->update('tips',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function delete_tip($id=''){
		if ($id != '') {
			$this->db->where('tipid',$id);
			return  $this->db->delete('tips');
		}
		return false;
	}
	public function update_tip_publish_status(){
		$this->db->where('tipid',$this->input->post('tip_id',true));
		return $this->db->update('tips',array('publish' => $this->input->post('publish',true)));
	}
	public function update_tip_position(){
		$this->db->where('tipid',$this->input->post('tip_id',true));
		return $this->db->update('tips',array('position' => $this->input->post('position',true)));
	}	
}