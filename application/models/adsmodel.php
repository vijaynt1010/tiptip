<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adsmodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_ads(){
		//$this->db->order_by('announcements.announcement','ASC');				
		return  $this->db->get('advertisement')->result();
	}	
	public function save_ad($data_array){
		if (is_array($data_array) && count($data_array) > 0 ) {
			if($this->db->insert('advertisement',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function get_ad_by_id($id = ''){
		if ($id != '') {
			$this->db->where('ad_id',$id);
			return  $this->db->get('advertisement')->row();
		}
		return false;		
	}
	
	public function update_ad($data_array = array()){
		if (is_array($data_array) && count($data_array) > 0 ) {
			$this->db->where('ad_id',$data_array['ad_id']);
			if($this->db->update('advertisement',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function delete_ad($id=''){
		if ($id != '') {
			$this->db->where('ad_id',$id);
			return  $this->db->delete('advertisement');
		}
		return false;
	}
	public function update_ad_status(){
		$this->db->where('ad_id',$this->input->post('ad_id',true));
		return $this->db->update('advertisement',array('status' => $this->input->post('status',true)));
	}
}