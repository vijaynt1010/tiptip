<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customersmodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_customers(){
		$this->db->_protect_identifiers = FALSE;
		$this->db->order_by('customer_name','ASC');
		$this->db->select("CONCAT(fname,' ',lname) customer_name",'ASC',false);		
		$return =   $this->db->get('user_details')->result();
		$this->db->_protect_identifiers = TRUE;
		return $return;
	}
	public function save_plan($data_array){
		if (is_array($data_array) && count($data_array) > 0 ) {
			if($this->db->insert('plans',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function get_plan_by_id($id = ''){
		if ($id != '') {
			$this->db->where('plan_id',$id);
			return  $this->db->get('plans')->row();
		}
		return false;		
	}
	
	public function update_plan($data_array = array()){
		if (is_array($data_array) && count($data_array) > 0 ) {
			$this->db->where('plan_id',$data_array['plan_id']);
			if($this->db->update('plans',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function delete_plan($id=''){
		if ($id != '') {
			$this->db->where('plan_id',$id);
			return  $this->db->delete('plans');
		}
		return false;
	}
	public function update_plan_status(){
		$this->db->where('plan_id',$this->input->post('plan_id',true));
		return $this->db->update('plans',array('status' => $this->input->post('status',true)));
	}
}