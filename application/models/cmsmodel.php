<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cmsmodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_cms(){
		//$this->db->order_by('announcements.announcement','ASC');				
		return  $this->db->get('cms')->result();
	}	
	public function save_Cms($data_array){
		if (is_array($data_array) && count($data_array) > 0 ) {
			if($this->db->insert('cms',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function get_cms_by_id($id = ''){
		if ($id != '') {
			$this->db->where('id',$id);
			return  $this->db->get('cms')->row();
		}
		return false;		
	}
	
	public function update_cms($data_array = array()){
		if (is_array($data_array) && count($data_array) > 0 ) {
			$this->db->where('id',$data_array['id']);
			if($this->db->update('cms',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function delete_cms($id=''){
		if ($id != '') {
			$this->db->where('id',$id);
			return  $this->db->delete('cms');
		}
		return false;
	}

	public function update_cms_status(){
		$this->db->where('id',$this->input->post('cm_id',true));
		return $this->db->update('cms',array('status' => $this->input->post('status',true)));
	}
}