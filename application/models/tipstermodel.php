<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tipstermodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_tipsters(){
		$this->db->order_by('tipster_name','ASC');
		return  $this->db->get('tipster')->result();
	}
	public function save_tipster($data_array){
		if (is_array($data_array) && count($data_array) > 0 ) {
			if($this->db->insert('tipster',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function get_tipster_by_id($id = ''){
		if ($id != '') {
			$this->db->where('id',$id);
			return  $this->db->get('tipster')->row();
		}
		return false;		
	}
	
	public function update_tipster($data_array = array()){
		if (is_array($data_array) && count($data_array) > 0 ) {
			$this->db->where('id',$data_array['id']);
			if($this->db->update('tipster',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function delete_tipster($id=''){
		if ($id != '') {
			$this->db->where('id',$id);
			return  $this->db->delete('tipster');
		}
		return false;
	}
	public function update_tipster_status(){
		$this->db->where('id',$this->input->post('tipster_id',true));
		return $this->db->update('tipster',array('tipster_status' => $this->input->post('status',true)));
	}
}