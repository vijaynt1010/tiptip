<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Announcementsmodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_announcements(){
		$this->db->order_by('announcements.announcement','ASC');				
		return  $this->db->get('announcements')->result();
	}	
	public function save_announcement($data_array){
		if (is_array($data_array) && count($data_array) > 0 ) {
			if($this->db->insert('announcements',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function get_announcement_by_id($id = ''){
		if ($id != '') {
			$this->db->where('announce_id',$id);
			return  $this->db->get('announcements')->row();
		}
		return false;		
	}
	
	public function update_announcement($data_array = array()){
		if (is_array($data_array) && count($data_array) > 0 ) {
			$this->db->where('announce_id',$data_array['announce_id']);
			if($this->db->update('announcements',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function delete_announcement($id=''){
		if ($id != '') {
			$this->db->where('announce_id',$id);
			return  $this->db->delete('announcements');
		}
		return false;
	}
	public function update_announcement_position(){
		$this->db->where('announce_id',$this->input->post('announce_id',true));
		return $this->db->update('announcements',array('position' => $this->input->post('position',true)));
	}
	public function update_announcement_status(){
		$this->db->where('announce_id',$this->input->post('announce_id',true));
		return $this->db->update('announcements',array('status' => $this->input->post('status',true)));
	}
}