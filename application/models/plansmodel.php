<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Plansmodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_plans(){
		$this->db->order_by('plan_name','ASC');
		return  $this->db->get('plans')->result();
	}
	public function save_plan($data_array){
		if (is_array($data_array) && count($data_array) > 0 ) {
			if($this->db->insert('plans',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function get_plan_by_id($id = ''){
		if ($id != '') {
			$this->db->where('plan_id',$id);
			return  $this->db->get('plans')->row();
		}
		return false;		
	}
	
	public function update_plan($data_array = array()){
		if (is_array($data_array) && count($data_array) > 0 ) {
			$this->db->where('plan_id',$data_array['plan_id']);
			if($this->db->update('plans',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function delete_plan($id=''){
		if ($id != '') {
			$this->db->where('plan_id',$id);
			return  $this->db->delete('plans');
		}
		return false;
	}
	public function update_plan_status(){
		$this->db->where('plan_id',$this->input->post('plan_id',true));
		return $this->db->update('plans',array('status' => $this->input->post('status',true)));
	}
}