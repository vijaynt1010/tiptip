<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH .'third_party/phpass-0.3/PasswordHash.php';
class Loginmodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function is_logged_in(){			
		if ('' != s('USER_ID') && null != s('USER_ID')) {
			return true;
		}
		return false;
	}
	
	public function process_login($login_data = array()){
		if (is_array($login_data) && count($login_data) > 0) {
			$t_hasher = new PasswordHash(8, FALSE);
			$this->db->select('id,first_name,last_name,user_name,user_password,email_id');
			$this->db->where('user_name',$login_data['user_name']);
			$this->db->where('user_status',1);
			$result_set = $this->db->get('admin_users');
			if ($result_set->num_rows() === 1) {
				$row = $result_set->row();
				$check = $t_hasher->CheckPassword($login_data['user_password'], $row->user_password);
				if ($check) {
					$this->session->set_userdata('USER_ID',$row->id);
					$this->session->set_userdata('USER_NAME',$row->user_name);
					return true;
				}
			}
		} 
		return false;
	}
	
	public function logout(){
		$this->session->set_userdata('USER_ID','');
		$this->session->set_userdata('USER_NAME','');	
		$this->session->unset_userdata('USER_ID');
		$this->session->unset_userdata('USER_NAME');
	}
	
	public function getProfile(){
		$this->db->select('id,first_name,last_name,user_name,email_id');
		$this->db->where('id',s('USER_ID'));		
		$result_set = $this->db->get('admin_users');	
		return $result_set->row();
	}
	
	public function changepassword($password_data = array()){
		if (is_array($password_data) && count($password_data) > 0) {			
			$t_hasher = new PasswordHash(8, FALSE);				
			$this->db->select('user_password');
			$this->db->where('id',s('USER_ID'));
			$result_set = $this->db->get('admin_users');
			
			if ($result_set->num_rows() == 1) {	
				$row = $result_set->row();
				$check = $t_hasher->CheckPassword($password_data['current_password'], $row->user_password);
				if ($check) {
					$hash = $t_hasher->HashPassword($password_data['new_password']);
					$this->db->where('id',s('USER_ID'));
					if($this->db->update('admin_users',array('user_password'=>$hash))){
						return true;
					}
				}
			}
			
		}
		return false;	
	}
	
	function doForgotPassword($login = NULL){
		if (!is_array ($login) || 0 >= count ($login))
		{
			return FALSE;
		}
		$useremail        = $login['user_email'];
		$this->db->where('email_id',$useremail);
		$this->db->where('user_status','1');
		$this->db->select('id,email_id,first_name,last_name,user_status');
		$this->db->from('admin_users');
		$result_set   = $this->db->get ();
		if (1 === $result_set->num_rows ())  {
			$row 		= $result_set->row();
			$this->db->select('uuid() as `key`');
			$uuid_resultset   = $this->db->get();
			$uudi = $uuid_resultset->row()->key;
	
			$expiary_date		= time()+24*60*60;
			$data		= array("password_reset_key"=>$uudi,"password_reset_key_validity"=>$expiary_date);
			$this->db->where('id', $row->id);
			$this->db->update('admin_users', $data);
	
			$resetPasswordLink = '';
			$resetPasswordLink .= base_url().'admin/reset/password/'.  base64_encode($uudi).'/'.  base64_encode($row->id);
			$subject                 = 'Password Reset';
// 			
	
			$body_content                  = 'Hi '.$row->first_name.',<br/>Reset you password <br>';
			$body_content                  .= 'Your attempt to reset password is success. Please follow the instructions.<br>';
			//$to_name                 = '';
			//$header_content          = '';
			$body_content            .= '<a href="'.$resetPasswordLink.'">Click here</a> to reset password <br> or <br> <br> Copy this link <br> '.$resetPasswordLink;
			//send_mail($head_1,$head_2,$to_name,$useremail, $subject,$header_content, $body_content,'Admin','vijaynt10102@gmail.com','','')  ;

			die($body_content);
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$this->email->from('service@tiptip.com', 'Service');
			$this->email->to($useremail);
			
			$this->email->subject($subject);
			$this->email->message($body_content);
			
			$this->email->send();			
			//echo $this->email->print_debugger();
			return TRUE;
		}
		return FALSE;
	
	}

	function doResetPassword($login = NULL){
		try {
			$t_hasher = new PasswordHash(8, FALSE);
	
			if (!is_array ($login) || 0 >= count ($login))
			{
				return FALSE;
			}
			$password_reset_key = base64_decode($login['password_reset_key']);
			$user_id            = base64_decode($login['user_id']);
			if ( ! preg_match("/^[a-z0-9:_\/-]+$/i", $user_id)) {
				throw new Exception;
			}
			$this->db->where('id',$user_id);
			if ( ! preg_match("/^[a-z0-9:_\/-]+$/i", $password_reset_key)) {
				throw new Exception;
			}
			$this->db->where('password_reset_key',$this->db->escape_str($password_reset_key));
			$this->db->where('password_reset_key_validity >=',time());
			$this->db->where('user_status','1');
			$this->db->select('id,user_status,email_id');
			$this->db->from('admin_users');
			$result_set   = $this->db->get ();
			if (1 === $result_set->num_rows ())
			{
	
				$row 			= $result_set->row();
				$password_hash = $t_hasher->HashPassword($login['user_newpassword']);
				if ( $password_hash ){
	
					$update_date		= time();
					$data		= array("user_password"=>$password_hash,"password_reset_key"=>NULL,"password_reset_key_validity"=>NULL);
					$this->db->where('id', $row->id);
					if( $this->db->update('admin_users', $data)){
						return true;
					}
				}
			}
	
			return false;
		} catch (Exception $ex) {
			 
			return false;
		}
	
	}
	
	function isValidResetKey($resetKey = NULL,$userId = NULL){
		try {
			if ($resetKey != NULL && $userId != NULL) {
				$password_reset_key = base64_decode($resetKey);
				$user_id            = base64_decode($userId);
				if ( ! preg_match("/^[a-z0-9:_\/-]+$/i", $user_id)) {
					throw new Exception;
				}
				$this->db->where('id',$user_id);
				if ( ! preg_match("/^[a-z0-9:_\/-]+$/i", $password_reset_key)) {
					throw new Exception;
				}
				$this->db->where('password_reset_key',$this->db->escape_str($password_reset_key));
				$this->db->where('password_reset_key_validity >=',time());
				$this->db->where('user_status','1');
				$this->db->select('id,user_status,email_id');
				$this->db->from('admin_users');
				$result_set   = $this->db->get ();
				if (1 === $result_set->num_rows ())
				{
					return TRUE;
				}
			}
			return FALSE;
		} catch (Exception $ex){
			 
			return false;
		}
	}
	
	
	public function redirect(){
		$uri = $this->CI->session->userdata('REQUEST_URI');
		$this->CI->session->set_userdata(array('REQUEST_URI' => ''));
		if (isset($uri) && "" != $uri && NULL != $uri) {
			redirect($uri);
		}else{
			$this->CI->redirect('admin/dashboard');
		}
	}
		
	public function hashPassword($user_password)
	{
		$t_hasher = new PasswordHash(8, FALSE);
		return $t_hasher->HashPassword($user_password);
	}	
	
	public function getProfileOfLoggedUser(){
		$this->db->select('id,first_name,last_name,user_name,email_id');
		$this->db->where('id',s('USER_ID'));
		$result_set = $this->db->get('admin_users');
		return $result_set->row();
	}	
	
	public function updateProfile($profile_data = array()){
		if (is_array($profile_data) && count($profile_data) > 0) {
			$this->db->where('id',$profile_data['id']);
			if($this->db->update('admin_users',$profile_data)){
				$profile = $this->getProfileOfLoggedUser();
				$this->session->set_userdata('USER_ID',$profile->id);
				$this->session->set_userdata('USER_NAME',$profile->user_name);
				return true;				
			}
		}
		return false;
	}
	
	public function is_username_available($user_name=''){
		if ('' != $user_name) {
			$this->db->select('id');
			$this->db->where('id !=',s('USER_ID'));
			$this->db->where('user_name',$user_name);
			$result_set = $this->db->get('admin_users');
			return $result_set->row();
		}
		return false;
	}
	
	public function is_email_available($email=''){
		if ('' != $email) {
			$this->db->select('id');
			$this->db->where('id !=',s('USER_ID'));
			$this->db->where('email_id',$email);
			$result_set = $this->db->get('admin_users');
			return $result_set->row();
		}
		return false;
	}	
}
