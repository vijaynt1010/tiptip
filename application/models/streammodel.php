<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Streammodel extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_streams(){
		$this->db->order_by('stream_code','ASC');
		return  $this->db->get('stream')->result();
	}
	public function save_stream($data_array){
		if (is_array($data_array) && count($data_array) > 0 ) {
			if($this->db->insert('stream',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function get_stream_by_id($id = ''){
		if ($id != '') {
			$this->db->where('id',$id);
			return  $this->db->get('stream')->row();
		}
		return false;		
	}
	
	public function update_stream($data_array = array()){
		if (is_array($data_array) && count($data_array) > 0 ) {
			$this->db->where('id',$data_array['id']);
			if($this->db->update('stream',$data_array)){
				return true;
			}
		}
		return false;
	}
	public function delete_stream($id=''){
		if ($id != '') {
			$this->db->where('id',$id);
			return  $this->db->delete('stream');
		}
		return false;
	}
	public function update_stream_status(){
		$this->db->where('id',$this->input->post('stream_id',true));
		return $this->db->update('stream',array('status' => $this->input->post('status',true)));
	}
}