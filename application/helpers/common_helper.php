<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * to access config settings easily
 * 
 */
function c($setting_name){
	$CI = &get_instance();
	return $CI->config->item($setting_name);
}

/**
 * to access content from session easily
 * 
 */
function s($item_name){

	//global $CI;
	$CI = &get_instance();

	return $CI->session->userdata($item_name);
}

/**
 * to access content from flash session easily
 * 
 */
function f($item_name){
	$CI = &get_instance();
	return $CI->session->flashdata($item_name);
}

/**
 * SET SESSION - to set an item to session easily
 * 
 */
function ss($item_name, $item_value){
	$CI = &get_instance();
	return $CI->session->set_userdata($item_name, $item_value);
}

/**
 * SET FLASH DATA - to set an item to session easily
 * 
 */
function sf($item_name, $item_value){
	$CI = &get_instance();
	return $CI->session->set_flashdata($item_name, $item_value);
}
/**
 * KEEP FLASH DATA - to keep an item to session easily
 * 
 */
function kf($item_name){
	$CI = &get_instance();
	return $CI->session->keep_flashdata($item_name);
}

function p($v = array()){
    echo '<pre>';
    print_r($v);
    echo '<pre>';
}
?>
