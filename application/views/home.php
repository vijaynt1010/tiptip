<!DOCTYPE html>
<html>
<head>
	<title>TipTip</title>
	<link rel="stylesheet" type="text/css" href="assets/frontend/css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="overall_area">
	<div class="header_area">
		<div class="header_top">
			<div class="logo floatleft">
				<img src="assets/frontend/images/logo.png">
			</div>
			<div class="header_content floatleft">
				<h3>TiPS PREDICTIONS FROM ALL OVER THE WORLD<br/><span>NOW IN ONE PLACE</span></h3>
				<p>tiptip.net</p>
				<h4>Find Your Tip & make your ticket</h4>
			</div>
			<!--========menu===========-->
			<div class="header_social floatleft">
				<div class="menu">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Login</a></li>
					</ul>
				</div>
				<!-- Social Login -->
				<div class="social_login">
					<div class="login_area">
						<div class="facebook_login floatleft">
							<p>LOGIN WITH FACEBOOK</p>
						</div>
						<div class="twitter_login floatright">
							<p>LOGIN WITH TWITTER</p>
						</div>
					</div>	
					<!--=============social images=========-->
					<div class="social_apps">
						<img src="assets/frontend/images/app_store.png" alt="error">
						<img src="assets/frontend/images/google_play.png" alt="error">
						<img src="assets/frontend/images/windows.png" alt="error">
					</div>
				</div>				
			</div>
		</div>	
		<div class="action_logo">
			<img src="assets/frontend/images/attension.png" alt="Error image">
		</div>
	</div>

		<!--===============content area start===============-->
	<div class="left_right_content">
			<div class="calculator floatleft">	
				<img src="assets/frontend/images/cal.png" alt="Calculator" title="Calculator "/><span>Calculator</span>
				<img src="assets/frontend/images/bookmark.png" alt="bookmark" title="bookmark "/><span>Bookmark</span>
				<img src="assets/frontend/images/following.png" alt="following" title="Calculator "/><span>Following</span>
			</div>	
			<div class="main_content_area floatleft">
				<div class="div_one_two">
					<div class="div_two floatleft">
						<div class="left_content_one">
							<div class="floatleft">
								<h2>My Tips</h2>
							</div>
							<div class="printer_area floatright">
								<img src="assets/frontend/images/massage.png">
								<img src="assets/frontend/images/printer.png">
							</div><br>
							<div class="conetnt_details">
								<div class="add_ticket">
									<h4>
										<span id="left_align"></span>
										<span >
												
										</span>
									</h4>
								</div>
								<div class="pre_one">								
									<h3>	<span class="fl">18:00</span>   <div class="w_197 fl">BARCELONA-RIAL MADRID</div>  odd:<span>2.3</span>  tip:<span style="color:#FF0105">2-2</span><input class="images_one" type="checkbox">
								</div>						
								<div class="pre_one">								
									<h3>	<span class="fl">18:00</span>   <div class="w_197 fl">BARCELONA-RIAL MADRID</div>  odd:<span>2.3</span>  tip:<span style="color:#FF0105">2-2</span><input class="images_one" type="checkbox">
								</div>									
							</div>
							<div class="total_amount">
								<h3><span></span></h3>
							</div>
							<div class="link">
								<a href="#" class="remove">Remove</a>
								<a href="#" class="trip">Add Your Tip</a>							
							</div>
						
						</div>				
					</div>			
					<div class="div_three_content floatleft">
						<img src="assets/frontend/images/arrow.png" alt="Arrow"/>
					</div>
					<div class="div_two floatleft">
						<div class="left_content_one">
							<div class="floatleft">
								<h2>My Tickets</h2>
							</div>
							<div class="printer_area floatright">
								<img src="assets/frontend/images/massage.png">
								<img src="assets/frontend/images/printer.png">
							</div><br>
							<div class="conetnt_details">
								<div class="add_ticket">
									<h4>
										<span id="left_align">1 <b>X </b> 2<b> X</b> 3<b> X</b></span>
										<span >
											<img src="assets/frontend/images/add.png"/> &nbsp;Add Ticket	
										</span>
									</h4>
								</div>
								<div class="pre_one">								
									<h3>	<span class="fl">18:00</span>   <div class="w_197 fl">BARCELONA-RIAL MADRID</div>  odd:<span>2.3</span>  tip:<span style="color:#FF0105">2-2</span><input class="images_one" type="checkbox">
								</div>						
								<div class="pre_one">								
									<h3>	<span class="fl">18:00</span>   <div class="w_197 fl">BARCELONA-RIAL MADRID</div>  odd:<span>2.3</span>  tip:<span style="color:#FF0105">2-2</span><input class="images_one" type="checkbox">
								</div>								
							</div>
							<div class="total_amount">
								<h3>Total: <span>6.90</span></h3>
							</div>
							<div class="link">
								<a href="#" class="remove">Remove</a>
								<a href="#" class="trip">Add Your Tip</a>							
							</div>
						
						</div>				
					</div>		
				</div>
				<!--=================logo area start=====================-->
				<div class="music_head">
					<div class="logo_action_head floatleft" >
						<h2>30-11-2014</h2>
					</div>
					<div class="floatleft music_image" >
						<img src="assets/frontend/images/music.png" alt="music image" title="Music" class="img_one" />
					</div>
				</div>
				<div class="logo_action">
					<img src="assets/frontend/images/attension.png" alt="music image" title="Music" class="img_two">
				</div>
			<!--========================Check_before_main start==========================-->
			<div class="bind_check_double">	
			<div class="Check_before_main floatleft">
					<h2>Check before you decide</h2>
					<div class="search">
						<h3>Tips</h3>
						<input type="text" placeholder="Find">
						<button>go</button>
					</div>	
					<div class="one one_color padding_t_6">
						<p>21.10.2014<span>Gorgav</span><img src="assets/frontend/images/NEW.gif" id="star"></p>
						<h4><span class="fl margin_l_0">18:00</span><div class="w_251 fl margin_t_4 margin_l_39">BARCELONA-VALENCIA</div><span><input type="checkbox"></span></h4>
						<div class="one_color_bottom">
						<img id="football"src="assets/frontend/images/football-icon.png" alt="Error">
						<b class="black margin_r_5px">odd:</b>2.30 <span class="follow_button">Follow</span> <b class="black margin_r_5px">tip:</b>2-2&3+ <span class="follow_button">Archive</span>
						</div>
					</div>
					<div class="one two_color padding_t_6" id="second">
						<p>21.10.2014<span>Gorgav</span><img src="assets/frontend/images/NEW.gif" id="star"></p>
						<h4><span class="fl margin_l_0">18:00</span><div class="w_251 fl margin_t_4 margin_l_39">BARCELONA-VALENCIA</div><span><input type="checkbox"></span></h4>
						<div class="one_color_bottom">
							<img id="football"src="assets/frontend/images/criketball.png" alt="Error">
							<b class="black margin_r_5px">odd:</b><strong class="white margin_r_5px">2.30 </strong><span class="archive_button">Follow</span> <b class="black">tip:</b><strong class="white">2-2&3+ </strong><span class="archive_button">Archive</span>		
						</div>						
					</div>
					<div class="one one_color padding_t_6">
						<p>21.10.2014<span>Gorgav</span><img src="assets/frontend/images/NEW.gif"  id="star"></p>
						<h4><span class="fl margin_l_0">18:00</span><div class="w_251 fl margin_t_4 margin_l_39">BARCELONA-VALENCIA</div><span><input type="checkbox"></span></h4>
						<div class="one_color_bottom">
						<img id="football"src="assets/frontend/images/football-icon.png" alt="Error">
						<b class="black margin_r_5px">odd:</b>2.30 <span class="follow_button">Follow</span> <b class="black margin_r_5px">tip:</b>2-2&3+ <span class="follow_button">Archive</span>
						</div>
					</div>
					<div class="one two_color padding_t_6" id="second">
						<p>21.10.2014<span>Gorgav</span></p>
						<h4><span class="fl margin_l_0">18:00</span><div class="w_251 fl margin_t_4 margin_l_39">BARCELONA-VALENCIA</div><span><input type="checkbox"></span></h4>
						<div class="one_color_bottom">
						<img id="football"src="assets/frontend/images/criketball.png" alt="Error">
						<b class="black margin_r_5px">odd:</b><strong class="white">2.30</strong> <span class="archive_button">Follow</span> <b class="black margin_r_5px">tip:</b><strong class="white">2-2&3+ </strong><span class="archive_button">Archive</span>	
						</div>
					</div>
					<div class="one live_bet">
						<h1>Live Bet Tip</h1>
							<p>Good Chances MAN UTD to store the next goal</p>						
					</div>
			</div>
			<!--=========================double_chance start================================-->
				<div class="double_chance floatleft">
					<h2>Double Check before you place you bet</h2>
					<div class="search">
						<h3>Tips</h3>
						<input type="text" name="text" id="margin_left" placeholder="Find">
						<button>go</button>
					</div>	
					<div class="one one_color double_marge padding_t_6">
						<p></p>
						<span><img src="assets/frontend/images/NEW.gif"></span>
						<h3></h3><input type="checkbox" class="checkbox">
						<div class="one_color_bottom">
						<img id="football"src="assets/frontend/images/football-icon.png" alt="Error">
						<b class="black margin_r_5px">odd:</b><strong class="white">2.30 </strong><span class="follow_button">Follow</span> <b class="black margin_r_5px">tip:</b><strong class="white">2-2&3+ </strong><span class="follow_button">Archive</span>
						</DIV>
					</div>
					<div class="one two_color double_marge padding_t_6" id="second">
						<p></p>
						<span><img src="assets/frontend/images/NEW.gif"></span>
						<h3></h3><input type="checkbox" class="checkbox">
						<div class="one_color_bottom">
						<img id="football"src="assets/frontend/images/criketball.png" alt="Error">
						
						<b class="black margin_r_5px">odd:</b><strong class="white">2.30</strong> <span class="archive_button">Follow</span> <b class="black margin_r_5px">tip:</b><strong class="white">2-2&3+ </strong><span class="archive_button">Archive</span>
						</div>						
					</div>
					<div class="one one_color double_marge3 padding_t_6">
						<p></p>
						<span><img src="assets/frontend/images/NEW.gif"></span>
						<h3></h3><input type="checkbox" class="checkbox">
						<div class="one_color_bottom">
						<img id="football"src="assets/frontend/images/football-icon.png" alt="Error">
						<b class="black margin_r_5px">odd:</b><strong class="white">2.30 </strong><span class="follow_button">Follow</span> <b class="black margin_r_5px">tip:</b><strong class="white">2-2&3+ </strong><span class="follow_button">Archive</span>
						</div >
					</div>
					<div class="one two_color double_marge padding_t_6" id="second">
						<p></p>
						
						<h3></h3><input type="checkbox" class="checkbox">
						<div class="one_color_bottom">
						<img id="football"src="assets/frontend/images/criketball.png" alt="Error">
						<b class="black margin_r_5px">odd:</b><strong class="white">2.30</strong> <span class="archive_button">Follow</span> <b class="black margin_r_5px">tip:</b><strong class="white">2-2&3+ </strong><span class="archive_button">Archive</span>	
</div>						
					</div>
					<div class="one live_bet_total" id="background_color">
						<div class="fill_out">
							<p></p>
							<h3></h3>
						</div>
						<div id="tips">
							<p>Odd:<span>2.30</span> Tip:<span>2-2.3+</span></p>		
						</div>
										
					</div>
				</div>	
				</div>	
				<!--==========================================-->
				<div class="change_double_area">
					<h2>Double Chance<span><img src="assets/frontend/images/NEW.gif" id="star"></span></h2>
					<div class="change_double floatleft">
						<p>18.00</p>
						<h3>barcelona-real madrid</h3>
						<h4>Odd:<span>1.45</span>&nbsp;&nbsp;&nbsp;&nbsp;tip:<span>1X</span></h4>
						<table>
							<tr>
								<td>1</td>
								<td>X</td>
								<td>2</td>
							</tr>
							<tr>
								<td>58%</td>
								<td>30%</td>
								<td>12%</td>
							</tr>

						</table>
						<p id="table_content">1X</p>
					</div>
					<div class="change_double floatleft">
						<p>18.00</p>
						<h3>barcelona-real madrid</h3>
						<h4>Odd:<span>1.45</span>&nbsp;&nbsp;&nbsp;&nbsp;tip:<span>1X</span></h4>
						<table>
							<tr>
								<td>1</td>
								<td>X</td>
								<td>2</td>
							</tr>
							<tr>
								<td>58%</td>
								<td>30%</td>
								<td>12%</td>
							</tr>

						</table>
						<p id="table_content">1X</p>
					</div>
					<div class="change_double floatleft">
						<p>18.00</p>
						<h3>barcelona-real madrid</h3>
						<h4>Odd:<span>1.45</span>&nbsp;&nbsp;&nbsp;&nbsp;tip:<span>1X</span></h4>
						<table>
							<tr>
								<td>1</td>
								<td>X</td>
								<td>2</td>
							</tr>
							<tr>
								<td>58%</td>
								<td>30%</td>
								<td>12%</td>
							</tr>

						</table>
						<p id="table_content">1X</p>
					</div>
					<div class="streams_area">
						<p>streams</p>
					</div>
				</div>
				<!---------------Footer Area-------------->
				<div class="footer_area">
		<div class="footer_top">
			<img src="assets/frontend/images/shoes.png" alt="not fund">	
		</div>
	
		<div class="footer_bottom">
			<div class="footer_left floatleft fix">
				<div class="foot floatleft fix">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Disclaimar</a></li>
						<li><a href="#">Terms and Condition</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">Login</a></li>						
						<li><a href="#">Help</a></li>
						<li><a href="#">Privacy</a></li>
					</ul>
				</div>				
			</div>
			<div class="footer_right floatleft fix">
				<img src="assets/frontend/images/windows.png" alt="not pund">
				<img src="assets/frontend/images/app_store.png" alt="not pund">
				<img src="assets/frontend/images/google_play.png" alt="not pund">
			</div>
		</div>									
	</div>
	
				
			</div>	
	<!--=================sideber start=====================-->
	<div class="div_three floatleft">
		<div class="some_content">
			<div class="promo_one fix">
				<p>NO MORE SPENDING<br/><span>50,$100,$200,$1000</span>AND <br/>MORE <span>10%</span> OR<span>1000000%</span><br/>FUARANTEED WIN</p>
			</div>
			<div class="promo_one border_no fix">
				<p>WE BUY TIPS AND PROVIDE EXCLUSIVE TO YOU IN THE WINDOW BELOW</p>
			</div>
			<div class="promo_one text_color fix">
				<p>PLUS <br/> -FILTRED TIPS, -1-2.2-1.7+..TIPS -LIVE BET TIPS, -ANALYSED TIPS AND MUCH MORE</p>
			</div>		
			<div class="promo_one border_no fix">
				<p><span>SUBSCRIBE</span> AND BECOME A MEMBER TODAY JUST FOR<br><br> <span>10.44</span> NOT PER DAY, NOT PER MONTH BUT PER YEAR<br/><button class="subscribe">SUBSCRIBE</button></p>
			</div>																				
		</div>
		<div class="sidebar_img fix">
			<img src="assets/frontend/images/rightside_attension.png" alt="not sapoted"/>
			<img id="shoe_height" src="assets/frontend/images/rightside_shoe.png" alt="not sapoted"/>
			<img id="footbal_height" src="assets/frontend/images/football.png" alt="not sapoted"/>
			<p>Payment Method</p>
			<img id="paypal_height" src="assets/frontend/images/paypal.png" alt="not sapoted"/>
		</div>

		<div class="sidebar_footer_top fix floatright">
			<h2>This is where you'll put the pay off.</h2>
		</div>
		<div class="sidebar_footer_bottom fix">
			<p>Copyright &copy;2011.</p>
		</div>	

		</div>		

	</div>
</body>
</html>