<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Stream - Update Form
							</h2>
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<form id="streamEditForm" name="streamEditForm" method="post" action="<?php echo base_url();?>admin/stream/edit/<?php echo $stream->id;?>">
								<div class="form-group">
									<label for="stream_code">Stream Code</label> <input
										type="text" placeholder="Stream Code" value="<?php echo set_value('stream_code',$stream->stream_code); ?>" name="stream_code" id="stream_code"
										class="form-control">
								</div>																												
								<div class="form-group">
									<label for="streamstatus">Status</label>
									<div class='input-group' id="streamstatus">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="stream_status_active" <?php echo set_radio('status', '1', ($stream->status=='1')?TRUE:FALSE); ?> name="status"> Active
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="0" id="stream_status_inactive" <?php echo set_radio('status', '0',($stream->status=='0')?TRUE:FALSE); ?> name="status"> Inactive
		                				</label>
	                				</div>	                				
	                			</div>															
								<button class="btn btn-primary" type="submit">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/stream'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>