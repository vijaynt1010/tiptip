<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>TipTip <?php echo $_title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="HelpValet <?php echo $_title;?>">

    <!-- The styles -->
    <link href="<?php echo base_url();?>assets/backend/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link id="bs-css" href="<?php echo base_url();?>assets/backend/css/bootstrap-cerulean.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/css/charisma-app.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/css/custom.css" rel="stylesheet">
    
	<?php if ( isset($_cssFiles) && count($_cssFiles) > 0 ):?>
		<?php foreach($_cssFiles as $_cssFile) :?>		
		<link href="<?php echo base_url().$_cssFile;?>" rel="stylesheet">
		<?php endforeach;?>
	<?php endif;?>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/backend/img/favicon.ico">
	<script type="text/javascript">
		var base_url = '<?php echo base_url();?>admin/';
	</script>
</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <a class="navbar-brand" href="<?php echo base_url();?>admin/dashboard">
            	<!--img alt="HelpValet" src="<?php echo base_url();?>assets/backend/img/logo.png" class="hidden-xs"/ -->
                <span>Tip Tip</span>
            </a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"><?php echo s('USER_NAME');?></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url();?>admin/myaccount/profile">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url();?>admin/myaccount/changepassword">Change Password</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url();?>admin/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <!--  div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div-->
            <!-- theme selector ends -->

            <!-- ul class="collapse navbar-collapse nav navbar-nav top-menu">
                <li><a href="#"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
                <li>
                    <form class="navbar-search pull-left">
                        <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                               type="text">
                    </form>
                </li>
            </ul-->

        </div>
    </div>
    <!-- topbar ends -->