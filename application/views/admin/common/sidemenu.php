<!-- left menu starts -->
<div class="col-sm-2 col-lg-2">
	<div class="sidebar-nav">
		<div class="nav-canvas">
			<ul class="nav nav-pills nav-stacked main-menu">
				<li class="nav-header">Main</li>
				<li><a class="ajax-link"
					href="<?php echo base_url();?>admin/dashboard"><i
						class="glyphicon glyphicon-home"></i><span> Dashboard</span></a></li>
				<li class="accordion"><a href="#"><i
						class="glyphicon glyphicon-book"></i><span> Tip Management</span></a>
					<ul class="nav nav-pills nav-stacked">
						<li><a href="<?php echo base_url();?>admin/tipster">Tipster</a></li>
						<li><a href="<?php echo base_url();?>admin/tips">All Tips</a></li>
						<li><a href="<?php echo base_url();?>admin/tips/left">Left Tips</a></li>
						<li><a href="<?php echo base_url();?>admin/tips/right">Right Tips</a></li>
						<li><a href="<?php echo base_url();?>admin/tips/archives">Archive Tips</a></li>
						<li><a href="<?php echo base_url();?>admin/doublechancetips">Double
								Chance Tips</a></li>
					</ul></li>
				<li><a class="ajax-link" href="<?php echo base_url();?>admin/plans"><i
						class="glyphicon glyphicon-book"></i><span> Plans</span></a></li>
				<li><a class="ajax-link"
					href="<?php echo base_url();?>admin/announcements"><i
						class="glyphicon glyphicon-book"></i><span> Announcements</span></a></li>
				<li><a class="ajax-link" href="<?php echo base_url();?>admin/stream"><i
						class="glyphicon glyphicon-book"></i><span> Stream</span></a></li>
				<li><a class="ajax-link" href="<?php echo base_url();?>admin/ads"><i
						class="glyphicon glyphicon-book"></i><span> Ad Management</span></a></li>
				<li><a class="ajax-link" href="<?php echo base_url();?>admin/cms"><i
						class="glyphicon glyphicon-book"></i><span> Website Contents</span></a></li>
			</ul>
		</div>
	</div>
</div>
<!--/span-->
<!-- left menu ends -->