    <footer class="row">
        <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="#" target="_blank">Tip Tip</a> <?php echo date('Y');?></p>

        <p class="col-md-3 col-sm-3 col-xs-12 powered-by">Powered by: <a
                href="#">Tip Tip</a></p>
    </footer>

</div><!--/.fluid-container-->

<!-- jQuery -->
<script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery-2.1.1.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

<?php if ( isset($_jsFiles) && count($_jsFiles) > 0 ):?>
	<?php foreach($_jsFiles as $_jsFile) :?>
	<script src="<?php echo base_url().$_jsFile;?>"></script>
	<?php endforeach;?>
<?php endif;?>
 <!-- script src="<?php echo base_url();?>assets/backend/js/customs/validation/validation.js"></script-->
<script src="<?php echo base_url();?>assets/backend/js/charisma.js"></script>


</body>
</html>
