<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Tipster - Update Form
							</h2>
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<form id="tipsterEditForm" name="tipsterEditForm" method="post" action="<?php echo base_url();?>admin/tipster/edit/<?php echo $tipster->id;?>">
								<div class="form-group">
									<label for="tipster_name">Tipster Name</label> <input
										type="text" placeholder="Tipster Name" value="<?php echo set_value('tipster_name',$tipster->tipster_name); ?>" name="tipster_name" id="tipster_name"
										class="form-control">
								</div>																												
								<div class="form-group">
									<label for="tipsterstatus">Tipster Status</label>
									<div class='input-group' id="tipsterstatus">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="tipster_status_active" <?php echo set_radio('tipster_status', '1', ($tipster->tipster_status=='1')?TRUE:FALSE); ?> name="tipster_status"> Active
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="0" id="tipster_status_inactive" <?php echo set_radio('tipster_status', '0',($tipster->tipster_status=='0')?TRUE:FALSE); ?> name="tipster_status"> Inactive
		                				</label>
	                				</div>	                				
	                			</div>															
								<button class="btn btn-primary" type="submit">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/tipster'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>