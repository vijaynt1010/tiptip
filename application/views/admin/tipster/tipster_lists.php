<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->

			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div class="box-header well" data-original-title="">
							<h2>
								<i class="glyphicon glyphicon-user"></i> Tipster
							</h2>

							<div class="box-icon">
								<a href="<?php echo base_url();?>admin/tipster/add" title="Add Tipster" class="btn btn-round btn-default"><i
								class="glyphicon glyphicon-plus"></i></a>
							</div>
						</div>
						<div class="box-content">
							<?php if ('' != $this->session->flashdata('error_message')):?>
							<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button><?php echo $this->session->flashdata('error_message')?></div>
							<?php endif;?>
							<?php if ('' != $this->session->flashdata('success_message')):?>
							<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button><?php echo $this->session->flashdata('success_message')?></div>
							<?php endif;?>							
							<table
								class="table table-striped table-bordered bootstrap-datatable datatable companyrep-table">
								<thead>
									<tr>
										<th>#</th>
										<th>Tipster Name</th>
										<th>Status</th>
										<th>&nbsp;</th>										
									</tr>
								</thead>
								<tbody>
								<?php $i=1; foreach($tipsters as $tipster) {?>
									<tr>
										<td><?php echo $i;?></td>	
										<td class="center"><?php echo $tipster->tipster_name;?></td>																																		
										<td class="center">
											<?php if ($tipster->tipster_status == 1):?>
											<span class="label-success label label-default" id="status-span-<?php echo $tipster->id?>">Active</span>
											<?php elseif($tipster->tipster_status == 0):?>
											<span class="label-danger label label-default" id="status-span-<?php echo $tipster->id?>">Inactive</span>
											<?php endif;?>
										</td>
										<td class="center">
										<a href="<?php echo base_url().'admin/tipster/edit/'.$tipster->id;?>" title="Click here to edit"> 
											<i class="glyphicon glyphicon-edit icon-white custom-icon-25"></i>
										</a>
										&nbsp;&nbsp;
										<?php if ($tipster->tipster_status == '1'):?>
										<a href="#" onclick="toggleStatus(this);" title="Click here to de-activate the tipster" data-tipster="<?php echo $tipster->id;?>" data-status="<?php echo '0';?>"> 
											<i class="glyphicon glyphicon-remove icon-white custom-icon-25"></i>
										</a>											
										<?php elseif($tipster->tipster_status == '0'):?>
										<a href="#" onclick="toggleStatus(this);" title="Click here to activate the tipster" data-tipster="<?php echo $tipster->id;?>" data-status="<?php echo '1';?>"> 
											<i class="glyphicon glyphicon-ok icon-white custom-icon-25"></i>
										</a>											
										<?php endif;?>											
										<a href="<?php echo base_url().'admin/tipster/delete/'.$tipster->id;?>" onclick="return confirm('Are you sure to remove the tipster?');" title="Clieck here to delete"> 
											<i class="glyphicon glyphicon-trash icon-white custom-icon-25"></i>
										</a>
										</td>
									</tr>
									<?php $i++; }?>
								</tbody>
							</table>

						</div>

					</div>
				</div>
			</div>
			<!-- content ends -->
		</div>
	</div>
	<script type="text/javascript">
	function toggleStatus(object) {
		var status     = $(object).attr('data-status');
		var tipster_id = $(object).attr('data-tipster');
		$.ajax({
			type	: 'post',
			url		: base_url+'tipster/changeStatus',
			data 	: {status : status , tipster_id : tipster_id},
			success	: function(response){
				if ( response.status == true ) {
					if ( status == '1' ) {
						$(object).find('i').removeClass('glyphicon-ok').addClass('glyphicon-remove');						
						$('#status-span-'+tipster_id).removeClass('label-danger').addClass('label-success');
						$('#status-span-'+tipster_id).html('Active');
						$(object).attr('data-status','0');
						$(object).attr('title','Click here to de-activate the tipster');																	
					} else if ( status == '0' ) {
						$(object).find('i').removeClass('glyphicon-remove').addClass('glyphicon-ok');
						$('#status-span-'+tipster_id).removeClass('label-success').addClass('label-danger');
						$('#status-span-'+tipster_id).html('Inactive');
						$(object).attr('data-status','1');
						$(object).attr('title','Click here to activate the tipster');												
					}
				} else if ( response.status == false ) {
					alert('An unknown error occured, try again!');
				}
			},
			error	: function() {
				alert('An unknown error occured, try again!');
			},
			dataType: 'json'
		});	
	}
	</script>	