<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Ad - Update Form
							</h2>
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<form id="adEditForm" name="adEditForm" method="post" action="<?php echo base_url();?>admin/ads/edit/<?php echo $ads->ad_id;?>">																
								<div class="form-group">
									<label for="ad_content">Ad Content</label> 
									<textarea name="ad_content" id="ad_content" class="form-control" placeholder="Ad Content"><?php echo $ads->content;?></textarea>									
								</div>																							
								<div class="form-group">
									<label for="adposition">Position</label>
									<div class='input-group' id="adposition">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="ad_position_top" <?php echo set_radio('position', '1', ($ads->position=='1')?TRUE:FALSE); ?> name="position"> Top
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="2" id="ad_position_right" <?php echo set_radio('position', '2',($ads->position=='2')?TRUE:FALSE); ?> name="position"> Right
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="3" id="ad_position_bottom" <?php echo set_radio('position', '3',($ads->position=='3')?TRUE:FALSE); ?> name="position"> Bottom
		                				</label>
		                				<label class="radio-inline">
		                    				<input type="radio" value="4" id="ad_position_left" <?php echo set_radio('position', '4',($ads->position=='4')?TRUE:FALSE); ?> name="position"> Left
		                				</label>
		                				<label class="radio-inline">
		                    				<input type="radio" value="5" id="ad_position_middle" <?php echo set_radio('position', '5',($ads->position=='5')?TRUE:FALSE); ?> name="position"> Middle
		                				</label>		                				
	                				</div>	                				
	                			</div>	
	                			<div class="form-group">
									<label for="adstatus">Status</label>
									<div class='input-group' id="adstatus">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="ad_status_active" <?php echo set_radio('status', '1', ($ads->status=='1')?TRUE:FALSE); ?> name="status"> Active
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="0" id="ad_status_inactive" <?php echo set_radio('status', '0',($ads->status=='0')?TRUE:FALSE); ?> name="status"> Inactive
		                				</label>												                			
	                				</div>	                				
	                			</div>	
								<button class="btn btn-primary" type="submit" onclick="return CKEditorAdsValidation();">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/ads'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>