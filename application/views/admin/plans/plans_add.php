<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Plans - Create Form
							</h2>
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<form id="planAddForm" name="planAddForm" method="post" action="<?php echo base_url();?>admin/plans/add">
								<div class="form-group">
									<label for="plan_name">Plan Name</label> <input
										type="text" placeholder="Plan Name" value="<?php echo set_value('plan_name'); ?>" name="plan_name" id="plan_name"
										class="form-control">
								</div>	
								<div class="form-group">
									<label for="plantype">Plan Type</label>
									<div class='input-group' id="plantype">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="plan_type_monthly" <?php echo set_radio('plan_type', '1', TRUE); ?> name="plan_type"> Monthly
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="2" id="plan_type_Yearly" <?php echo set_radio('plan_type', '2'); ?> name="plan_type"> Yearly
		                				</label>
	                				</div>										
								</div>
								<div class="form-group">
									<label for="plan_name">Plan Amount</label> <input
										type="text" placeholder="Plan Amount" value="<?php echo set_value('amount'); ?>" name="amount" id="amount"
										class="form-control">
								</div>
								<div class="form-group">
									<label for="planstatus">Status</label>
									<div class='input-group' id="planstatus">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="plan_status_active" <?php echo set_radio('status', '1', TRUE); ?> name="status"> Active
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="0" id="plan_status_inactive" <?php echo set_radio('status', '0'); ?> name="status"> Inactive
		                				</label>
	                				</div>	                				
	                			</div>															
								<button class="btn btn-primary" type="submit">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/plans/'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>