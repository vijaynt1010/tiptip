<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Myaccount - Change Password
							</h2>

							<!-- div class="box-icon">
								<a class="btn btn-setting btn-round btn-default" href="#"><i
									class="glyphicon glyphicon-cog"></i></a> <a
									class="btn btn-minimize btn-round btn-default" href="#"><i
									class="glyphicon glyphicon-chevron-up"></i></a> <a
									class="btn btn-close btn-round btn-default" href="#"><i
									class="glyphicon glyphicon-remove"></i></a>
							</div-->
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<?php if ($this->session->flashdata('success_message')):?>
							<div class="alert alert-success">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Success!</strong><?php echo $this->session->flashdata('success_message');?>
                			</div>
                			<?php elseif($this->session->flashdata('error_message')):?>							
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Success!</strong><?php echo $this->session->flashdata('error_message');?>
                			</div>                			             		
                			<?php endif;?>                			
							<form id="changePasswordForm" name="changePasswordForm" method="post" action="<?php echo base_url();?>admin/myaccount/changepassword">
								<div class="form-group">
									<label for="current_password">Current Password</label> <input
										type="password" placeholder="Current Password" value="" name="current_password" id="current_password"
										class="form-control">
								</div>							
								<div class="form-group">
									<label for="new_password">New Password</label> <input
										type="password" placeholder="New Password" value="" name="new_password" id="new_password"
										class="form-control">
								</div>
								<div class="form-group">
									<label for="confirm_newpassword">Monthly Price</label> <input
										type="password" placeholder="Confirm Password" value="" name="confirm_newpassword"
										id="confirm_newpassword" class="form-control">
								</div>																						
								<button class="btn btn-primary" type="submit">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/dashboard'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>