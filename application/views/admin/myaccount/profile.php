<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Myaccount - Profile
							</h2>

							<!-- div class="box-icon">
								<a class="btn btn-setting btn-round btn-default" href="#"><i
									class="glyphicon glyphicon-cog"></i></a> <a
									class="btn btn-minimize btn-round btn-default" href="#"><i
									class="glyphicon glyphicon-chevron-up"></i></a> <a
									class="btn btn-close btn-round btn-default" href="#"><i
									class="glyphicon glyphicon-remove"></i></a>
							</div-->
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<?php if ($this->session->flashdata('success_message')):?>
							<div class="alert alert-success">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Success!</strong><?php echo $this->session->flashdata('success_message');?>
                			</div>
                			<?php elseif($this->session->flashdata('error_message')):?>							
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Success!</strong><?php echo $this->session->flashdata('error_message');?>
                			</div>                			             		
                			<?php endif;?>                			
							<form id="profileUpdateForm" name="profileUpdateForm" method="post" action="<?php echo base_url();?>admin/myaccount/profile">
								<div class="form-group">
									<label for="first_name">First Name</label> <input
										type="text" placeholder="First Name" value="<?php echo set_value('first_name',$profile->first_name);?>" name="first_name" id="first_name"
										class="form-control">
								</div>							
								<div class="form-group">
									<label for="last_name">Last Name</label> <input
										type="text" placeholder="Last Name" value="<?php echo set_value('last_name',$profile->last_name);?>" name="last_name" id="last_name"
										class="form-control">
								</div>
								<div class="form-group">
									<label for="user_name">User Name</label> <input
										type="text" placeholder="User Name" value="<?php echo set_value('user_name',$profile->user_name);?>" name="user_name" id="user_name"
										class="form-control">
								</div>
								<div class="form-group">
									<label for="email_id">Email</label> <input
										type="text" placeholder="Email" value="<?php echo set_value('email_id',$profile->email_id);?>" name="email_id" id="email_id"
										class="form-control">
								</div>																					
								<button class="btn btn-primary" type="submit">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/dashboard'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>