<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Announcements - Create Form
							</h2>
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<form id="announcementsAddForm" name="announcementsAddForm" method="post" action="<?php echo base_url();?>admin/announcements/add">																
								<div class="form-group">
									<label for="steam">Announcement</label> 
									<textarea name="announcement" id="announcement" class="form-control" placeholder="Announcements"></textarea>									
								</div>								
								<div class="form-group">
									<label for="tip">Tip</label> 
									<input type="text" name="tip" id="tip" class="form-control" placeholder="Tip">
								</div>
								<div class="form-group">
									<label for="odd">Odd</label> 
									<input type="text" name="odd" id="odd" class="form-control" placeholder="Odd">
								</div>								
								<div class="form-group">
									<label for="tipposition">Position</label>
									<div class='input-group' id="tipposition">
										<label class="radio-inline">
		                    				<input type="radio" value="0"  id="tip_position_left" <?php echo set_radio('position', '0', TRUE); ?> name="position"> Left
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="1" id="tip_position_rigth" <?php echo set_radio('position', '1'); ?> name="position"> Right
		                				</label>
	                				</div>	                				
	                			</div>
	                			<div class="form-group">
									<label for="tipstatus">Status</label>
									<div class='input-group' id="tipstatus">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="tip_status_active" <?php echo set_radio('status', '1', TRUE); ?> name="status"> Active
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="0" id="tip_status_inactive" <?php echo set_radio('status', '0'); ?> name="status"> Inactive
		                				</label>												                			
	                				</div>	                				
	                			</div>									                		
								<button class="btn btn-primary" type="submit" onclick="return CKEditorAnnounceValidation();">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/announcements'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>