<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Announcements - Update Form
							</h2>
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<form id="announcementsEditForm" name="announcementsEditForm" method="post" action="<?php echo base_url();?>admin/announcements/edit/<?php echo $announcement->announce_id?>">																
								<div class="form-group">
									<label for="steam">Announcement</label> 
									<textarea name="announcement" id="announcement" class="form-control" placeholder="Announcements"><?php echo $announcement->announcement;?></textarea>									
								</div>								
								<div class="form-group">
									<label for="tip">Tip</label> 
									<input type="text" name="tip" id="tip" value="<?php echo $announcement->tip;?>" class="form-control" placeholder="Tip">
								</div>
								<div class="form-group">
									<label for="odd">Odd</label> 
									<input type="text" name="odd" id="odd" value="<?php echo $announcement->odd;?>" class="form-control" placeholder="Odd">
								</div>								
								<div class="form-group">
									<label for="announcementposition">Position</label>
									<div class='input-group' id="announcementposition">
										<label class="radio-inline">
		                    				<input type="radio" value="0"  id="announcement_position_left" <?php echo set_radio('position', '0', ($announcement->position == '0')?TRUE:FALSE); ?> name="position"> Left
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="1" id="announcement_position_rigth" <?php echo set_radio('position', '1',($announcement->position == '1')?TRUE:FALSE); ?> name="position"> Right
		                				</label>
	                				</div>	                				
	                			</div>
	                			<div class="form-group">
									<label for="announcementstatus">Status</label>
									<div class='input-group' id="announcementstatus">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="announcement_status_active" <?php echo set_radio('status', '1', TRUE,($announcement->status == '1')?TRUE:FALSE); ?> name="status"> Active
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="0" id="announcement_status_inactive" <?php echo set_radio('status', '1',($announcement->status == '0')?TRUE:FALSE); ?> name="status"> Inactive
		                				</label>
											                			
	                				</div>	                				
	                			</div>										                		
								<button class="btn btn-primary" type="submit" onclick="return CKEditorAnnounceValidation();">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/announcements'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>