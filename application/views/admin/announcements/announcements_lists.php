<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->

			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div class="box-header well" data-original-title="">
							<h2>
								<i class="glyphicon glyphicon-user"></i> Tips
							</h2>

							<div class="box-icon">
								<a href="<?php echo base_url();?>admin/announcements/add" title="Add Announcement" class="btn btn-round btn-default"><i
								class="glyphicon glyphicon-plus"></i></a>
							</div>
						</div>
						<div class="box-content">
							<?php if ('' != $this->session->flashdata('error_message')):?>
							<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button><?php echo $this->session->flashdata('error_message')?></div>
							<?php endif;?>
							<?php if ('' != $this->session->flashdata('success_message')):?>
							<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button><?php echo $this->session->flashdata('success_message')?></div>
							<?php endif;?>							
							<table
								class="table table-striped table-bordered bootstrap-datatable datatable companyrep-table">
								<thead>
									<tr>
										<th>#</th>
										<th>Announcement</th>
										<th>Tip</th>
										<th>Odd</th>
										<th>Position</th>
										<th>Status</th>
										<th>&nbsp;</th>										
									</tr>
								</thead>
								<tbody>
								<?php $i=1; foreach($announcements as $announcement) {?>
									<tr>
										<td><?php echo $i;?></td>	
										
										<td class="center"><?php echo (strlen($announcement->announcement)>20)? htmlentities(substr($announcement->announcement, 0,17)).'...' : htmlentities($announcement->announcement) ;?></td>												
										<td class="center"><?php echo $announcement->tip;?></td>
										<td class="center"><?php echo $announcement->odd;?></td>
										<td class="center" id="td-position-<?php echo $announcement->announce_id;?>">
										<?php if ($announcement->position == '0'):?>
										Left
										<?php elseif ($announcement->position == '1'):?>
										Right
										<?php endif;?>										
										</td>
										<td class="center">
											<?php if ($announcement->status == '0'):?>
											<span class="label label-danger" id="status-span-<?php echo $announcement->announce_id?>">Inactive</span>
											<?php elseif($announcement->status == '1'):?>
											<span class="label-success label label-default" id="status-span-<?php echo $announcement->announce_id?>">Active</span>										
											<?php endif;?>																				
										</td>
										<td class="center">
										
										<a href="<?php echo base_url().'admin/announcements/edit/'.$announcement->announce_id;?>"> 
											<i class="glyphicon glyphicon-edit icon-white"></i>
										</a>																													
										<?php if ($announcement->status == '1'):?>
										<a href="#" onclick="toggleStatus(this);" title="Click here to de-activate the announcement" data-announcement="<?php echo $announcement->announce_id;?>" data-status="<?php echo '0';?>"> 
											<i class="glyphicon glyphicon-remove icon-white"></i>
										</a>											
										<?php elseif($announcement->status == '0'):?>
										<a href="#" onclick="toggleStatus(this);" title="Click here to activate the announcement" data-announcement="<?php echo $announcement->announce_id;?>" data-status="<?php echo '1';?>"> 
											<i class="glyphicon glyphicon-ok icon-white"></i>
										</a>											
										<?php endif;?>										
										<?php if ($announcement->position == '1'):?>
										<a href="#" onclick="togglePosition(this);" title="Click here to move the announcement to left" data-announcement="<?php echo $announcement->announce_id;?>" data-position="<?php echo '0';?>"> 
											<i class="glyphicon glyphicon-arrow-left icon-white"></i>
										</a>											
										<?php elseif($announcement->position == '0'):?>
										<a href="#" onclick="togglePosition(this);" title="Click here to move the announcement to right" data-announcement="<?php echo $announcement->announce_id;?>" data-position="<?php echo '1';?>"> 
											<i class="glyphicon glyphicon-arrow-right icon-white"></i>
										</a>											
										<?php endif;?>
										
										<a href="<?php echo base_url().'admin/announcements/delete/'.$announcement->announce_id;?>" onclick="return confirm('Are you sure to remove the announcement?');"> 
											<i class="glyphicon glyphicon-trash icon-white"></i>
										</a>
										
										</td>
									</tr>
									<?php $i++; }?>
								</tbody>
							</table>

						</div>

					</div>
				</div>
			</div>
			<!-- content ends -->
		</div>
	</div>
	<script type="text/javascript">	
	function toggleStatus(object) {
		var status     = $(object).attr('data-status');
		var announce_id = $(object).attr('data-announcement');
		$.ajax({
			type	: 'post',
			url		: base_url+'announcements/changeStatus',
			data 	: {status : status , announce_id : announce_id},
			success	: function(response){
				if ( response.status == true ) {
					if ( status == '1' ) {
						$(object).find('i').removeClass('glyphicon-ok').addClass('glyphicon-remove');						
						$('#status-span-'+announce_id).removeClass('label-danger').addClass('label-success');
						$('#status-span-'+announce_id).html('Active');
						$(object).attr('data-status','0');
						$(object).attr('title','Click here to de-activate the announcement');																	
					} else if ( status == '0' ) {
						$(object).find('i').removeClass('glyphicon-remove').addClass('glyphicon-ok');
						$('#status-span-'+announce_id).removeClass('label-success').addClass('label-danger');
						$('#status-span-'+announce_id).html('Inactive');
						$(object).attr('data-status','1');
						$(object).attr('title','Click here to activate the announcement');												
					}
				} else if ( response.status == false ) {
					alert('An unknown error occured, try again!');
				}
			},
			error	: function() {
				alert('An unknown error occured, try again!');
			},
			dataType: 'json'
		});	
	}	
	function togglePosition(object) {
		var position = $(object).attr('data-position');
		var announce_id  = $(object).attr('data-announcement');
		$.ajax({
			type	: 'post',
			url		: base_url+'announcements/changePosition',
			data 	: {position : position , announce_id : announce_id},
			success	: function(response){
				if ( response.status == true ) {
					if ( position == '1' ) {
						$(object).find('i').removeClass('glyphicon-arrow-right').addClass('glyphicon-arrow-left');						
						//$('#status-span-'+tipster_id).removeClass('label-danger').addClass('label-success');
						//$('#status-span-'+tipster_id).html('Active');
						$('#td-position-'+announce_id).html('Right');
						$(object).attr('data-position','0');
						$(object).attr('title','Click here to move the announcement to left');																	
					} else if ( position == '0' ) {
						$(object).find('i').removeClass('glyphicon-arrow-left').addClass('glyphicon-arrow-right');
						//$('#status-span-'+tipster_id).removeClass('label-success').addClass('label-danger');
						//$('#status-span-'+tipster_id).html('Inactive');
						$('#td-position-'+announce_id).html('Left');
						$(object).attr('data-position','1');
						$(object).attr('title','Click here to move the announcement to right');												
					}
				} else if ( response.status == false ) {
					alert('An unknown error occured, try again!');
				}
			},
			error	: function() {
				alert('An unknown error occured, try again!');
			},
			dataType: 'json'
		});	
	}	
	</script>	