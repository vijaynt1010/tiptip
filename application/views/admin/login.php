<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>Tip Tip Admin Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Tip Tip - Admin Login">  
    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/css/charisma-app.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css" rel="stylesheet">    
	<script type="text/javascript">
		var base_url = '<?php echo base_url();?>';
	</script>
    

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
<div class="ch-container">
    <div class="row">
        
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Welcome to Tip Tip</h2>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row" id="loginform-div" <?php if( isset($form_action) && $form_action == 'forgotpassword') echo 'style="display:none;"'?>>
        <div class="well col-md-5 center login-box">
        <?php if ($this->session->flashdata('error_message')):?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error_message');?>
            </div>
        <?php elseif ($this->session->flashdata('success_message')):?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success_message');?>
            </div>        
        <?php else:?>
            <div class="alert alert-info">
                Please login with User Name & Password.
            </div>        
        <?php endif;?>
            <form class="form-horizontal" name="loginForm" id="loginForm" action="<?php echo base_url();?>admin/login" method="post">
                <fieldset>
                	<div class="form-group">
	                    <div class="input-group input-group-lg">
	                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
	                        <input type="text" name="user_name" autocomplete="off" id="user_name" class="form-control" placeholder="Username">
	                    </div>
                    </div>
                    <div class="clearfix"></div><br>
					<div class="form-group">
	                    <div class="input-group input-group-lg">
	                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
	                        <input type="password" name="user_password" id="user_password" class="form-control" placeholder="Password">
	                    </div>
                    </div>
                    <div class="clearfix"></div>
					<br/>
                    <div class="input-prepend">
                       <a href ="#" class="login_fp"> Forgot Password</a>
                    </div>
                    <div class="clearfix"></div>               

                    <p class="center col-md-5">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </p>
                </fieldset>
            </form>
        </div>
        <!--/span-->
    </div><!--/row-->
    
    
	<!--Forgot Password--->
	<div class="row" id="forgotpasswordform-div" <?php if( !isset($form_action) || $form_action != 'forgotpassword') echo 'style="display:none;"'?>>
	        <div class="well col-md-5 center login-box">
	        <?php if ($this->session->flashdata('forgot_error_message')):?>
	            <div class="alert alert-danger">
	                <?php echo $this->session->flashdata('forgot_error_message');?>
	            </div>
	        <?php elseif ($this->session->flashdata('forgot_success_message')):?>
	            <div class="alert alert-success">
	                <?php echo $this->session->flashdata('forgot_success_message');?>
	            </div>        
	        <?php else:?>
	            <div class="alert alert-info">
	                Please enter the email id.
	            </div>        
	        <?php endif;?>	        
	            <form class="form-horizontal" id="forgotPasswordForm" action="<?php echo base_url();?>admin/login/forgotpassword" method="post">
	                <fieldset>
	                    <h4> Forgot Password </h4>
	                    <br/>
	                                         
	                    <div class="form-group">
	                        <div class="input-group input-group-lg">
	                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope red"></i></span>
	                            <input type="text" name="user_email" autocomplete="off"  class="form-control" placeholder="Email">
	                        </div>
	                    </div>
	                    <div class="clearfix"></div><br>
	                    <div class="input-prepend">
	                        
	                        <a class ="login_lg" href ="#"> Back to login</a>
	                    </div>
	                    <p class="center col-md-5">
	                        <button type="submit" class="btn btn-primary">Reset</button>
	                    </p>
	                </fieldset>
	            </form>
	        </div>
	</div> 
	<!--Forgot Password--->    
    
</div><!--/fluid-row-->

</div><!--/.fluid-container-->

<!-- jQuery -->
<script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/customs/validation/validation.js"></script>
<!-- application script for Charisma demo -->
<!-- script src="<?php echo base_url();?>assets/backend/js/charisma.js"></script-->
<script type="text/javascript">
$(document).ready(function(){
    $('.login_fp').click(function(){
        $('#loginform-div').slideUp('fast');
        $('#forgotpasswordform-div').slideDown('slow');
    });
    $('.login_lg').click(function(){
        $('#forgotpasswordform-div').slideUp('fast');
        $('#loginform-div').slideDown('slow');
    });
      
    $('#forgotPasswordForm').bootstrapValidator({        
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            user_email: {
                message: 'The email is not valid',
                validators: {
                    notEmpty: {

                        message: 'The email is required'
                    },emailAddress: {
                        message: 'Please enter a valid email'

                    }
                }
            }
        }
    });    
});
//  $('#user_name').on('input', function(evt) {                                                                       // Convert to uppercase
//               $(this).val(function (_, val) {
//                 return val.toUpperCase();
//               });
//             })

$('#user_name').bind("paste",function(e) {                                                                         // Disable Ctrl+V in User name Field     
                  e.preventDefault();
});
</script>
</body>
</html>
