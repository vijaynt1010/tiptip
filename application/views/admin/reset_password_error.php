<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>Tip Tip Reset Password Error</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Tip Tip - Reset Password Error">  
    <!-- The styles -->
    <link id="bs-css" href="<?php echo base_url();?>assets/backend/css/bootstrap-cerulean.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/css/charisma-app.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    	<script type="text/javascript">
		var base_url = '<?php echo base_url();?>';
	</script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.ico">

</head>

<body>
<div class="ch-container">
    <div class="row">
        
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Tip Tip - Reset Password</h2>
        </div>
        <!--/span-->
    </div><!--/row-->
<!--Reset Password Form--->
<div class="row" id="resetpasswordform-div">
        <div class="well col-md-5 center login-box">
            <form class="form-horizontal" id="resetPasswordForm" action="<?php echo base_url();?>admin/reset/password/<?php echo $reset_key?>/<?php echo $user_id?>" method="post">
                <fieldset>
                    <h4 class="title"> Reset Password </h4>
                    <?php if (isset($error_message) && '' != $error_message){?>
                    <div class="alert alert-danger">
                        <!--button class="close" data-dismiss="alert" type="button">×</button-->
                        <strong> <?php echo $error_message; ?>      </strong>
                    </div>   
                <?php }?>  
                    <div class="clearfix"></div>

                    <div class="input-prepend">                        
                        <a class ="login_fp" href ="<?php echo base_url();?>login"> Back to login</a>
                    </div>                    
                </fieldset>
            </form>
        </div>
</div>
<!--Reset Password Form--->
</div><!--/fluid-row-->

</div><!--/.fluid-container-->

<!-- jQuery -->
<script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- application script for Charisma demo -->
<!-- script src="<?php echo base_url();?>assets/backend/js/charisma.js"></script-->
</body>
</html>