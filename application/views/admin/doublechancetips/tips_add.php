<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Double Chance Tip - Create Form
							</h2>
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<form id="doubleChanceTipsAddForm" name="doubleChanceTipsAddForm" method="post" action="<?php echo base_url();?>admin/doublechancetips/add">
								<div class="form-group">
									<label for="tip">Tip</label> 
									<input type="text" name="tip" id="tip" class="form-control" placeholder="Tip">
								</div>
								<div class="form-group">
									<label for="odd">Odd</label> 
									<input type="text" name="odd" id="odd" class="form-control" placeholder="Odd">
								</div>	
								<div class="form-group">
									<label for="time">Time</label> 
									<input type="text" name="time" id="time" class="form-control" placeholder="H:i">
								</div>
								<div class="form-group">
									<label for="fteam">First Team</label> 
									<input type="text" name="fteam" id="fteam" class="form-control" placeholder="First Team">
								</div>
								<div class="form-group">
									<label for="steam">Second Team</label> 
									<input type="text" name="steam" id="steam" class="form-control" placeholder="Second Team">
								</div>								
								<div class="form-group">
									<label for="row_onef1">Row 11</label> 
									<input type="text" name="row_onef1" id="row_onef1" class="form-control" placeholder="Row 11">
								</div>
								<div class="form-group">
									<label for="row_onef2">Row 12</label> 
									<input type="text" name="row_onef2" id="row_onef2" class="form-control" placeholder="Row 12">
								</div>		
								<div class="form-group">
									<label for="row_onef3">Row 13</label> 
									<input type="text" name="row_onef3" id="row_onef3" class="form-control" placeholder="Row 13">
								</div>	
								<div class="form-group">
									<label for="row_twof1">Row 21</label> 
									<input type="text" name="row_twof1" id="row_twof1" class="form-control" placeholder="Row 21">
								</div>
								<div class="form-group">
									<label for="row_twof2">Row 22</label> 
									<input type="text" name="row_twof2" id="row_twof2" class="form-control" placeholder="Row 22">
								</div>		
								<div class="form-group">
									<label for="row_twof3">Row 23</label> 
									<input type="text" name="row_twof3" id="row_twof3" class="form-control" placeholder="Row 23">
								</div>	
								<div class="form-group">
									<label for="btm_column">Between Column</label> 
									<input type="text" name="btm_column" id="btm_column" class="form-control" placeholder="Between Column">
								</div>	
	                			<div class="form-group">
									<label for="tipstatus">Status</label>
									<div class='input-group' id="tipstatus">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="tip_status_active" <?php echo set_radio('status', '1', TRUE); ?> name="status"> Active
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="0" id="tip_status_inactive" <?php echo set_radio('status', '0'); ?> name="status"> Inactive
		                				</label>		                				
	                				</div>	                				
	                			</div>	                			
								<button class="btn btn-primary" type="submit">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/doublechancetips'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>