<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Tip - Update Form
							</h2>
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<form id="doubleChanceTipsEditForm" name="doubleChanceTipsEditForm" method="post" action="<?php echo base_url();?>admin/doublechancetips/edit/<?php echo $tip->id?>">
								<div class="form-group">
									<label for="tip">Tip</label> 
									<input type="text" name="tip" id="tip" class="form-control" placeholder="Tip" value="<?php echo set_value('tip',$tip->tip);?>">
								</div>
								<div class="form-group">
									<label for="odd">Odd</label> 
									<input type="text" name="odd" id="odd" class="form-control" placeholder="Odd" value="<?php echo set_value('odd',$tip->odd);?>">
								</div>	
								<div class="form-group">
									<label for="time">Time</label> 
									<input type="text" name="time" id="time" class="form-control" placeholder="H:i" value="<?php echo set_value('time',$tip->time);?>">
								</div>
								<div class="form-group">
									<label for="fteam">First Team</label> 
									<input type="text" name="fteam" id="fteam" class="form-control" placeholder="First Team" value="<?php echo set_value('fteam',$tip->fteam);?>">
								</div>
								<div class="form-group">
									<label for="steam">Second Team</label> 
									<input type="text" name="steam" id="steam" class="form-control" placeholder="Second Team" value="<?php echo set_value('steam',$tip->steam);?>">
								</div>								
								<div class="form-group">
									<label for="row_onef1">Row 11</label> 
									<input type="text" name="row_onef1" id="row_onef1" class="form-control" placeholder="Row 11" value="<?php echo set_value('row_onef1',$tip->row_onef1);?>">
								</div>
								<div class="form-group">
									<label for="row_onef2">Row 12</label> 
									<input type="text" name="row_onef2" id="row_onef2" class="form-control" placeholder="Row 12" value="<?php echo set_value('row_onef2',$tip->row_onef2);?>">
								</div>		
								<div class="form-group">
									<label for="row_onef3">Row 13</label> 
									<input type="text" name="row_onef3" id="row_onef3" class="form-control" placeholder="Row 13" value="<?php echo set_value('row_onef3',$tip->row_onef3);?>">
								</div>	
								<div class="form-group">
									<label for="row_twof1">Row 21</label> 
									<input type="text" name="row_twof1" id="row_twof1" class="form-control" placeholder="Row 21" value="<?php echo set_value('row_twof1',$tip->row_twof1);?>">
								</div>
								<div class="form-group">
									<label for="row_twof2">Row 22</label> 
									<input type="text" name="row_twof2" id="row_twof2" class="form-control" placeholder="Row 22" value="<?php echo set_value('row_twof2',$tip->row_twof2);?>">
								</div>		
								<div class="form-group">
									<label for="row_twof3">Row 23</label> 
									<input type="text" name="row_twof3" id="row_twof3" class="form-control" placeholder="Row 23" value="<?php echo set_value('row_twof3',$tip->row_twof3);?>">
								</div>	
								<div class="form-group">
									<label for="btm_column">Between Column</label> 
									<input type="text" name="btm_column" id="btm_column" class="form-control" placeholder="Between Column" value="<?php echo set_value('btm_column',$tip->btm_column);?>">
								</div>	
	                			<div class="form-group">
									<label for="tipstatus">Status</label>
									<div class='input-group' id="tipstatus">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="tip_status_active" <?php echo set_radio('status', '1', ($tip->status=='1')?TRUE:FALSE); ?> name="status"> Active
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="0" id="tip_status_inactive" <?php echo set_radio('status', '0',($tip->status=='0')?TRUE:FALSE); ?> name="status"> Inactive
		                				</label>		                				
	                				</div>	                				
	                			</div>	                			
								<button class="btn btn-primary" type="submit">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/doublechancetips'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>