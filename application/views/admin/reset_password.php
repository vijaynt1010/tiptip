<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>Tip Tip Reset Password</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="KalPoints - Reset Password">  
    <!-- The styles -->
    <link id="bs-css" href="<?php echo base_url();?>assets/backend/css/bootstrap-cerulean.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/css/charisma-app.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/backend/plugins/bootstrapvalidator/dist/css/bootstrapValidator.min.css" rel="stylesheet">    

    	<script type="text/javascript">
		var base_url = '<?php echo base_url();?>';
	</script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.ico">

</head>

<body>
<div class="ch-container">
    <div class="row">
        
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Tip Tip - Reset Password</h2>
        </div>
        <!--/span-->
    </div><!--/row-->
<!--Reset Password Form--->
<div class="row" id="resetpasswordform-div">
        <div class="well col-md-5 center login-box">
            <form class="form-horizontal" id="resetPasswordForm" action="<?php echo base_url();?>admin/reset/password/<?php echo $reset_key?>/<?php echo $user_id?>" method="post">
                <fieldset>
                    <h4> Reset Password </h4> <br>
                    <?php if ("" != validation_errors()) {?>
                    <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert" type="button">×</button>
                    <strong> <?php echo validation_errors(); ?> </strong>
                    </div>
                    <?php }?>
                    <div class="form-group">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                            <input type="password" name="user_newpassword" class="form-control" autocomplete="off" placeholder="New Password">
                        </div>
                    </div>
                    <div class="clearfix"></div><br>
                    <div class="form-group">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                            <input type="password" name="user_confirmpassword" class="form-control" autocomplete="off" placeholder="Retype Password">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <p class="center col-md-5">
                        <button type="submit" class="btn btn-primary">Reset</button>
                    </p>
                </fieldset>
            </form>
        </div>
</div>
<!--Reset Password Form--->
</div><!--/fluid-row-->

</div><!--/.fluid-container-->

<!-- jQuery -->
<script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrapvalidator/dist/js/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/customs/validation/validation.js"></script>
<!-- application script for Charisma demo -->
<!-- script src="<?php echo base_url();?>assets/backend/js/charisma.js"></script-->
<script type="text/javascript">
$(document).ready(function(){

    
    $('#resetPasswordForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            user_newpassword: {
                message: 'The password is not valid',
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    },
                    securePassword : {
                        message: 'The password is not valid'
                    }
                }
            },
            user_confirmpassword: {
                validators: {
                    notEmpty: {
                        message: 'Retype password the new password'
                    },
//                    securePassword : {
//                        message: 'The password is not valid'
//                    },
                    identical : {
                        field : 'user_newpassword',
                        message: 'The passwords is do not match'
                    }
                }
            }
        }
    });         
});
</script>
</body>
</html>
