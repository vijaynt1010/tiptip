<div class="ch-container">
	<div class="row">
		<?php $this->load->view('admin/common/sidemenu');?>
		<div class="col-lg-10 col-sm-10" id="content">
			<!-- content starts -->
			
			<!-- div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Forms</a></li>
				</ul>
			</div-->

			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div data-original-title="" class="box-header well">
						
							<h2>
								<i class="glyphicon glyphicon-edit"></i> Tip - Update Form
							</h2>
						</div>
						<div class="box-content">
							<?php if (isset($error_message)):?>
							<div class="alert alert-danger">
                    			<button data-dismiss="alert" class="close" type="button">�</button>
                    			<strong>Error!</strong><?php echo $error_message;?>
                			</div>
                			<?php endif;?>
							<form id="tipsEditForm" name="tipsEditForm" method="post" action="<?php echo base_url();?>admin/tips/edit/<?php echo $tip->tipid?>">
								<div class="form-group">
									<label for="tipster_name">Tipster Name</label> 
									<select name="tipster_id" id="tipster_id" class="form-control selectpicker" placeholder="Tipster Name" data-size="10" data-live-search="true">
										<option value="">Select Tipster</option>
										<?php if ($tipsters):?>
											<?php foreach($tipsters as $tipster):?>
											<option <?php echo ($tip->tipster_id==$tipster->id)?'selected="selected"':'';?> value="<?php echo $tipster->id;?>"><?php echo $tipster->tipster_name;?></option>
											<?php endforeach;?>
										<?php endif;?>
									</select>
								</div>
								<div class="form-group">
									<label for="date">Date</label> 
									<input type="text" name="date" id="date" class="form-control" value="<?php echo date('m/d/Y',strtotime($tip->date));?>" placeholder="MM/DD/YYYY">
								</div>	
								<div class="form-group">
									<label for="time">Time</label> 
									<input type="text" name="time" id="time" value="<?php echo date('H:i',strtotime($tip->time));?>" class="form-control" placeholder="H:i">
								</div>
								<div class="form-group">
									<label for="fteam">First Team</label> 
									<input type="text" name="fteam" id="fteam" value="<?php echo $tip->fteam;?>" class="form-control" placeholder="First Team">
								</div>
								<div class="form-group">
									<label for="steam">Second Team</label> 
									<input type="text" name="steam" id="steam" value="<?php echo $tip->steam;?>" class="form-control" placeholder="Second Team">
								</div>								
								<div class="form-group">
									<label for="tip">Tip</label> 
									<input type="text" name="tip" id="tip" value="<?php echo $tip->tip;?>" class="form-control" placeholder="Tip">
								</div>
								<div class="form-group">
									<label for="odd">Odd</label> 
									<input type="text" name="odd" id="odd" value="<?php echo $tip->odd;?>" class="form-control" placeholder="Odd">
								</div>								
								<div class="form-group">
									<label for="tipposition">Position</label>
									<div class='input-group' id="tipposition">
										<label class="radio-inline">
		                    				<input type="radio" value="0"  id="tip_position_left" <?php echo set_radio('position', '0', ($tip->position == '0')?TRUE:FALSE); ?> name="position"> Left
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="1" id="tip_position_rigth" <?php echo set_radio('position', '1',($tip->position == '1')?TRUE:FALSE); ?> name="position"> Right
		                				</label>
	                				</div>	                				
	                			</div>
	                			<div class="form-group">
									<label for="tipstatus">Status</label>
									<div class='input-group' id="tipstatus">
										<label class="radio-inline">
		                    				<input type="radio" value="0"  id="tip_status_default" <?php echo set_radio('status', '0', TRUE,($tip->status == '0')?TRUE:FALSE); ?> name="status"> Default
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="1" id="tip_status_win" <?php echo set_radio('status', '1',($tip->status == '1')?TRUE:FALSE); ?> name="status"> Win
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="2" id="tip_status_lose" <?php echo set_radio('status', '2',($tip->status == '2')?TRUE:FALSE); ?> name="status"> Lose
		                				</label>		                				
	                				</div>	                				
	                			</div>
								<div class="form-group">
									<label for="tippublish">Publish</label>
									<div class='input-group' id="tippublish">
										<label class="radio-inline">
		                    				<input type="radio" value="1"  id="tip_publish" <?php echo set_radio('publish', '1',($tip->publish == '1')?TRUE:FALSE); ?> name="publish"> Not Publish
		                				</label>
										<label class="radio-inline">
		                    				<input type="radio" value="0" id="tip_not_publish" <?php echo set_radio('publish', '0',($tip->publish == '0')?TRUE:FALSE); ?> name="publish"> Publish
		                				</label>
	                				</div>	                				
	                			</div>		                			
								<button class="btn btn-primary" type="submit">Submit</button>
								<button class="btn btn-danger" type="button" onclick="window.location='<?php echo base_url();?>admin/tips'">Cancel</button>
							</form>

						</div>
					</div>
				</div>
				<!--/span-->

			</div>
			<!--/row-->

			<!-- content ends -->
		</div>
	</div>