<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['ads_position'] = array(
		"1" => "Top",
		"2" => "Right",
		"3" => "Bottom",
		"4" => "Left",
		"5" => "Middle"
);

$config['cms_position'] = array(
		"1" => "Top",
		"2" => "Right",
		"3" => "Bottom",
		"4" => "Left",
		"5" => "Middle"
);

$config['plan_types'] = array(
		"1" => "Monthly",
		"2" => "Yearly",
);